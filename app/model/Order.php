<?php

namespace app\model;

use app\core\BaseModel;

/**
 * 认购订单
 */
class Order extends BaseModel
{
    protected $name = 'order';
    protected $pk = 'order_id';
}