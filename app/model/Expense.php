<?php

namespace app\model;

use app\core\BaseModel;

class Expense extends BaseModel
{
    protected $name = 'expense';
    protected $pk = 'expense_id';
}