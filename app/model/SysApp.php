<?php

namespace app\model;

use app\core\BaseModel;

/**
 * 应用表
 */
class SysApp extends BaseModel
{
    protected $name = 'sys_app';
    protected $pk = 'app_id';
    protected $globalScope = [];
}