<?php

namespace app\model;

use app\core\BaseModel;

class TempleIncome extends BaseModel
{
    protected $name = 'temple_income';
    protected $pk = 'id';

}