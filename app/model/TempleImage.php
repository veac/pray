<?php

namespace app\model;

use app\core\BaseModel;

class TempleImage extends BaseModel
{
    protected $name = 'temple_image';
    protected $pk = 'id';

}