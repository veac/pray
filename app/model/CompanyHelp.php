<?php

namespace app\model;

use app\core\BaseModel;

class CompanyHelp extends BaseModel
{
    protected $name = 'company_help';
    protected $pk = 'id';

    public function company()
    {
        return $this->hasOne(Company::class, 'company_id', 'company_id');
    }
    public function interview()
    {
        return $this->hasOne(Interview::class, 'company_id', 'company_id');
    }
}