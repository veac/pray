<?php

namespace app\model;

use app\core\BaseModel;

class Product extends BaseModel
{
    protected $name = 'product';
    protected $pk = 'product_id';
}