<?php

namespace app\model;

use app\core\BaseModel;

class Agent extends BaseModel
{
    protected $name = 'agent';
    protected $pk = 'id';
}