<?php

namespace app\model;

use app\core\BaseModel;

class MemberInfo extends BaseModel
{
    protected $name = 'member_info';
    protected $pk = 'id';
}