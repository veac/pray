<?php

namespace app\model;

use app\core\BaseModel;

class SysPermission extends BaseModel
{
    protected $name = 'sys_permission';
    protected $pk = 'permission_id';

    /**
     * 角色 N
     */
    public function roles(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(SysRole::class, SysPermission::class, 'role_id', 'permission_id');
    }
}