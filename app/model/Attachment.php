<?php

namespace app\model;

use app\core\BaseModel;

class Attachment extends BaseModel
{
    protected $name = 'attachment';
    protected $pk = 'attachment_id';
}