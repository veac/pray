<?php

namespace app\model;

use app\core\BaseModel;

class Temple extends BaseModel
{
    protected $name = 'temple';
    protected $pk = 'temple_id';
}