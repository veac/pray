<?php

namespace app\model;

use app\core\BaseModel;

class SysLoginLog extends BaseModel
{
    protected $name = 'sys_login_log';
    protected $pk = 'login_log_id';

    protected $autoWriteTimestamp = false;
    protected $autoWriteBy = false;
}