<?php

namespace app\model;

use app\core\BaseModel;
use think\model\relation\HasOne;

class Company extends BaseModel
{
    protected $name = 'company';
    protected $pk = 'company_id';

    /**
     * 用户
     */
    public function user(): HasOne
    {
        return $this->hasOne(SysUser::class, 'user_id', 'user_id');
    }

    public function interview()
    {
        return $this->hasOne(Interview::class, 'lead_company_id', 'lead_company_id');
    }
}