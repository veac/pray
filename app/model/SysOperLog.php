<?php

namespace app\model;

use app\core\BaseModel;

class SysOperLog extends BaseModel
{
    protected $name = 'sys_oper_log';
    protected $pk = 'oper_id';

    protected $autoWriteTimestamp = false;
    protected $autoWriteBy = false;
}