<?php

namespace app\model;

use app\core\BaseModel;
use think\model\relation\HasOne;

class Appeal extends BaseModel
{
    protected $name = 'appeal';
    protected $pk = 'id';

    /**
     * 用户
     */
    public function user(): HasOne
    {
        return $this->hasOne(SysUser::class, 'user_id', 'user_id');
    }

}