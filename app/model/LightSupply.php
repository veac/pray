<?php

namespace app\model;

use app\core\BaseModel;

class LightSupply extends BaseModel
{
    protected $name = 'light_supply';
    protected $pk = 'id';
}