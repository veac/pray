<?php

namespace app\model;

use app\core\BaseModel;

class SysPost extends BaseModel
{
    protected $name = 'sys_post';
    protected $pk = 'post_id';
}