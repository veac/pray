<?php

namespace app\model;

use app\core\BaseModel;

class TempleWithdraw extends BaseModel
{
    protected $name = 'temple_withdraw';
    protected $pk = 'id';
}