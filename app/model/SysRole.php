<?php

namespace app\model;

use app\core\BaseModel;
use think\model\relation\BelongsToMany;

/**
 * 角色模型
 */
class SysRole extends BaseModel
{
    protected $name = 'sys_role';
    protected $pk = 'role_id';

    /**
     * 用户 N
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(SysUser::class, SysUserRole::class, 'user_id', 'role_id');
    }

    /**
     * 菜单 N
     */
    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(SysMenu::class, SysRoleMenu::class, 'menu_id', 'role_id');
    }

    /**
     * 权限 N
     */
    public function perms(): BelongsToMany
    {
        return $this->belongsToMany(SysPermission::class, SysRolePerm::class, 'permission_id', 'role_id');
    }

}