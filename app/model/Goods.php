<?php

namespace app\model;

use app\core\BaseModel;

class Goods extends BaseModel
{
    protected $name = 'goods';
    protected $pk = 'goods_id';
}