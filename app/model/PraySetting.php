<?php

namespace app\model;

use app\core\BaseModel;

class PraySetting extends BaseModel
{
    protected $name = 'pray_setting';
    protected $pk = 'id';
}