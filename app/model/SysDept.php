<?php

namespace app\model;

use app\core\BaseModel;

/**
 * 部门模型
 */
class SysDept extends BaseModel
{
    protected $name = 'sys_dept';
    protected $pk = 'dept_id';
}