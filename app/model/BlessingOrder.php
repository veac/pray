<?php

namespace app\model;

use app\core\BaseModel;

class BlessingOrder extends BaseModel
{
    protected $name = 'blessing_order';
    protected $pk = 'id';
}