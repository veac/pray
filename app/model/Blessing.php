<?php

namespace app\model;

use app\core\BaseModel;

class Blessing extends BaseModel
{
    protected $name = 'blessing';
    protected $pk = 'id';
}
