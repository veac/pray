<?php

namespace app\model;

use think\model\Pivot;

/**
 * 角色部门关联模型
 */
class SysRoleDept extends Pivot
{
    protected $name = 'sys_role_dept';
}