<?php

namespace app\model;

use app\core\BaseModel;

class HousePrice extends BaseModel
{
    protected $name = 'house_price';
    protected $pk = 'id';
}