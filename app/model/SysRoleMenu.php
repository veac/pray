<?php

namespace app\model;


use think\model\Pivot;

/**
 * 角色菜单中间模型
 */
class SysRoleMenu extends Pivot
{
    protected $name = 'sys_role_menu';
}