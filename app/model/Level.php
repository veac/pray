<?php

namespace app\model;

use app\core\BaseModel;

class Level extends BaseModel
{
    protected $name = 'level';
    protected $pk = 'id';
}