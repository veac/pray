<?php

namespace app\model;

use app\core\BaseModel;

class Cate extends BaseModel
{
    protected $name = 'cate';
    protected $pk = 'cate_id';
}