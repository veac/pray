<?php

namespace app\model;

use app\core\BaseModel;

class Gdp extends BaseModel
{
    protected $name = 'gdp';
    protected $pk = 'id';
}