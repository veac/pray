<?php

namespace app\model;


use app\core\BaseModel;
use think\model\relation\BelongsToMany;

/**
 * 用户模型
 */
class SysUser extends BaseModel
{
    protected $name = 'sys_user';
    protected $pk = 'user_id';

    /**
     * 角色 N
     */
    public function roles()
    {
        return $this->belongsToMany(SysRole::class, SysUserRole::class, 'role_id', 'user_id');
    }

    /**
     * 岗位 N
     */
    public function posts()
    {
        return $this->belongsToMany(SysPost::class, SysUserPost::class, 'post_id', 'user_id');
    }

    /**
     * 部门 1
     */
    public function dept()
    {
        return $this->hasOne(SysDept::class, 'dept_id', 'dept_id');
    }
}