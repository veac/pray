<?php

namespace app\model;

use think\model\Pivot;

/**
 * 用户角色中间模型
 */
class SysUserRole extends Pivot
{
    protected $name = 'sys_user_role';
}