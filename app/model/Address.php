<?php

namespace app\model;

use app\core\BaseModel;

class Address extends BaseModel
{
    protected $name = 'address';
    protected $pk = 'address_id';
}