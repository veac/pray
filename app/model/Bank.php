<?php

namespace app\model;

use app\core\BaseModel;

class Bank extends BaseModel
{
    protected $name = 'bank';
    protected $pk = 'id';
}
