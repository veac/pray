<?php

namespace app\model;

use app\core\BaseModel;

class Member extends BaseModel
{
    protected $name = 'member';
    protected $pk = 'member_id';
}