<?php

namespace app\model;

use app\core\BaseModel;
use think\model\relation\HasOne;

class ZtcUser extends BaseModel
{
    protected $autoWriteTimestamp = false;
    protected $autoWriteBy = false;

    /**
     * 用户
     */
    public function user(): HasOne
    {
        return $this->hasOne(SysUser::class, 'user_id', 'user_id');
    }
}