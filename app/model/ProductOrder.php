<?php

namespace app\model;

use app\core\BaseModel;

class ProductOrder extends BaseModel
{
    protected $name = 'product_order';
    protected $pk = 'order_id';
}