<?php

namespace app\model;

use app\core\BaseModel;

class TempleSetting extends BaseModel
{
    protected $name = 'temple_setting';
    protected $pk = 'id';
}