<?php

namespace app\model;

use app\core\BaseModel;

class Category extends BaseModel
{
    protected $name = 'category';
    protected $pk = 'category_id';
}