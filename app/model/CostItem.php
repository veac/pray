<?php

namespace app\model;

use app\core\BaseModel;

class CostItem extends BaseModel
{
    protected $name = 'cost_item';
    protected $pk = 'id';
}
