<?php

namespace app\model;

use app\core\BaseModel;

class Banner extends BaseModel
{
    protected $name = 'banner';
    protected $pk = 'id';
}
