<?php

namespace app\model;


use app\core\BaseModel;

class TempleBank extends BaseModel
{
    protected $name = 'temple_bank';
    protected $pk = 'id';
}