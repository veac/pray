<?php

namespace app\model;

use app\core\BaseModel;

class ProductOrderDetail extends BaseModel
{
    protected $name = 'product_order_detail';
    protected $pk = 'id';
}