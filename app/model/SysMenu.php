<?php

namespace app\model;

use app\core\BaseModel;

/**
 * 菜单模型
 */
class SysMenu extends BaseModel
{
    protected $name = 'sys_menu';
    protected $pk = 'menu_id';
    protected $globalScope = [];

    /**
     * 角色 N
     */
    public function roles(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(SysRole::class, SysRoleMenu::class, 'role_id', 'menu_id');
    }
}