<?php
// 全局中间件定义文件

use app\core\middleware\AllowCrossDomain;

return [
    // 全局跨域
    AllowCrossDomain::class
//    // 全局请求缓存
//    CheckRequestCache::class,
//    // 多语言加载
//    LoadLangPack::class,
//    // Session初始化
//    SessionInit::class
];
