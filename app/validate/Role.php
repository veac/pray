<?php

namespace app\validate;

use app\core\Constant;
use think\Validate;

class Role extends Validate
{
    protected $rule = [
        'role_id' => 'require',
        'role_name' => 'require',
        'role_key' => 'require',
        'data_scope' => 'checkDataScope',
    ];

    protected $message = [
        'role_id' => '缺少角色ID',
        'role_name' => '缺少角色名称',
        'role_key' => '缺少角色标识',
        'data_scope' => '数据权限错误'
    ];

    protected $scene = [
        'create' => ['role_name', 'role_key'],
        'update' => ['role_id', 'role_name', 'role_key'],
    ];

    protected function checkDataScope($val)
    {
        if ($val == Constant::DATA_SCOPE_ALL) {
            return true;
        }
        if ($val == Constant::DATA_SCOPE_DIV) {
            return true;
        }
        if ($val == Constant::DATA_SCOPE_DEPT) {
            return true;
        }
        if ($val == Constant::DATA_SCOPE_DEPTS) {
            return true;
        }
        if ($val == Constant::DATA_SCOPE_SELF) {
            return true;
        }
        return false;
    }
}