<?php

namespace app\validate;

use think\Validate;

class Expense extends Validate
{
    protected $rule = [
        'title' => 'require',
        'money' => 'require',
        'category' => 'require',
        'expense_time' => 'require',
    ];

    protected $message = [
        'title' => '缺少费用明细',
        'money' => '缺少费用金额',
        'category' => '缺少费用类别',
        'expense_time' => '缺少费用产生时间',
    ];

    protected $scene = [
        'create' => ['title', 'money', 'category', 'expense_time'],
        'update' => ['title', 'money', 'category', 'expense_time'],
    ];
}