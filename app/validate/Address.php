<?php

namespace app\validate;

use think\Validate;

class Address extends Validate
{
    protected $rule = [
        'name' => 'require',
        'mobile' => 'require',
        'address' => 'require',
        'detail_address' => 'require',
    ];

    protected $message = [
        'name' => '请输入姓名',
        'mobile' => '请输入手机号',
        'address' => '请输入地址信息',
        'detail_address' => '请输入详细地址信息',
    ];

    protected $scene = [
        'save' => ['name', 'mobile', 'address', 'detail_address'],
    ];
}