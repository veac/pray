<?php

namespace app\validate;

use think\Validate;

class Post extends Validate
{
    protected $rule = [
        'post_id' => 'require',
        'post_code' => 'require',
        'post_name' => 'require',
    ];

    protected $message = [
        'post_id' => '缺少岗位ID',
        'post_code' => '缺少岗位编码',
        'post_name' => '缺少岗位名称',
    ];

    protected $scene = [
        'create' => ['post_code', 'post_name'],
        'update' => ['post_id', 'post_code', 'post_name'],
    ];
}