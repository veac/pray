<?php

namespace app\validate;

use think\Validate;

class Menu extends Validate
{
    protected $rule = [
        'menu_id' => 'require',
        'app_key' => 'require',
        'menu_name' => 'require',
        'status' => 'require',
    ];

    protected $message = [
        'menu_id' => '缺少菜单ID',
        'app_key' => '缺少应用标识',
        'menu_name' => '缺少菜单名称',
        'status' => '缺少菜单状态',
    ];

    protected $scene = [
        'create' => ['app_key', 'menu_name', 'status'],
        'update' => ['app_id', 'app_key', 'menu_name', 'status'],
    ];
}