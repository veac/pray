<?php

namespace app\validate;

use think\Validate;

class App extends Validate
{
    protected $rule = [
        'app_id' => 'require',
        'app_key' => 'require',
        'app_name' => 'require',
    ];

    protected $message = [
        'app_id' => '缺少ID',
        'app_key' => '缺少应用标记',
        'app_name' => '缺少应用名称',
    ];

    protected $scene = [
        'create' => ['app_key', 'app_name'],
        'update' => ['app_id', 'app_key', 'app_name'],
    ];
}