<?php

namespace app\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'user_id' => 'require',
        'user_name' => 'require',
        'password' => 'require|min:6',
        'nick_name' => 'require',
        'dept_id' => 'require',
    ];

    protected $message = [
        'user_id' => '缺少用户ID',
        'user_name' => '请输入帐号',
        'password' => '请输入密码',
        'password.min' => '密码至少6位',
        'nick_name' => '缺少昵称',
    ];

    protected $scene = [
        'login' => ['user_name', 'password'],
        'create' => ['user_name', 'password', 'nick_name'],
        'update' => ['user_id', 'user_name', 'nick_name']
    ];

}
