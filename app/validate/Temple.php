<?php


namespace app\validate;


use think\Validate;

class Temple extends Validate
{

    protected $rule = [
        'account' => 'require',
        'password' => 'require|min:6',
    ];

    protected $message = [
        'account' => '请输入账号',
        'password' => '请输入密码',

    ];

    protected $scene = [
        'login' => ['account', 'password'],
      ];
}