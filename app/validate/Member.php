<?php

namespace app\validate;

use think\Validate;

class Member extends Validate
{
    protected $rule = [
        'member_id' => 'require',
        'mobile' => 'require',
        'password' => 'require|min:6',
        'ret_password' => 'require|min:6',
        'real_name' => 'require',
        'id_card' => 'require',
        'bank_card' => 'require',
        'bank_open' => 'require',
        'bank_branch' => 'require',
        'id_card1' => 'require',
        'id_card2' => 'require',
    ];

    protected $message = [
        'member_id' => '缺少ID',
        'mobile' => '请输入手机号',
        'password' => '请输入密码',
        'password.min' => '密码至少6位',
        'ret_password' => '请重复输入密码',
        'ret_password.min' => '密码至少6位',
        'real_name' => '请输入姓名',
        'id_card' => '请输入身份证号',
        'bank_card' => '请输入银行卡号',
        'bank_open' => '请选择开户行',
        'bank_branch' => '请输入开户支行',
        'id_card1' => '请上传身份证正面',
        'id_card2' => '请上传身份证反面',

    ];

    protected $scene = [
        'login' => ['mobile', 'password'],
        'create' => ['mobile', 'password'],
        'reset' => ['password', 'ret_password'],
        'code' => ['mobile'],
        'real_name' => ['real_name', 'id_card', 'bank_card', 'bank_open', 'bank_branch', 'mobile', 'id_card1', 'id_card2'],
    ];

}
