<?php

namespace app\validate;

use think\Validate;

class Activity extends Validate
{
    protected $rule = [
        'title' => 'require',
        'company_name' => 'require',
    ];

    protected $message = [
        'title' => '缺少活动标题',
        'company_name' => '缺少公司名称',
    ];

    protected $scene = [
        'create' => ['title', 'company_name'],
        'update' => ['title', 'company_name'],
    ];
}