<?php

namespace app\validate;

use think\Validate;

class Permission extends Validate
{
    protected $rule = [
        'permission_id' => 'require',
        'app_key' => 'require',
        'name' => 'require',
        'status' => 'require',
    ];

    protected $message = [
        'permission_id' => '缺少权限ID',
        'app_key' => '缺少应用标识',
        'name' => '缺少名称',
        'status' => '缺少权限状态',
    ];

    protected $scene = [
        'create' => ['app_key', 'name', 'status'],
        'update' => ['permission_id', 'app_key', 'name', 'status'],
    ];
}