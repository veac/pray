<?php

namespace app\validate;

use think\Validate;

class Company extends Validate
{
    protected $rule = [
        'company_name' => 'require',
        'company_code' => 'require',
    ];

    protected $message = [
        'company_name' => '缺少企业名称',
        'company_code' => '缺少统一社会信用代码',
    ];

    protected $scene = [
        'create' => ['company_name', 'company_code'],
        'update' => ['company_name', 'company_code'],
    ];
}