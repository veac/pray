<?php

namespace app\validate;

use think\Validate;

class Dept extends Validate
{
    protected $rule = [
        'dept_id' => 'require',
        'parent_id' => 'require',
        'dept_name' => 'require',
    ];

    protected $message = [
        'dept_id' => '缺少部门ID',
        'parent_id' => '缺少上级部门',
        'dept_name' => '缺少部门名称',
    ];

    protected $scene = [
        'create' => ['parent_id', 'dept_name'],
        'update' => ['dept_id', 'parent_id', 'dept_name'],
    ];
}