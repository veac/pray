<?php

namespace app\validate;

use think\Validate;

class ZtcUser extends Validate
{
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'code' => 'require',
    ];

    protected $message = [
        'username' => '缺少账号',
        'password' => '缺少密码',
        'code' => '缺少图形验证码',
    ];

    protected $scene = [
        'code' => ['username'],
        'bind' => ['username', 'password', 'code'],
    ];
}