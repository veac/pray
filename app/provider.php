<?php

// 容器Provider定义文件
use app\core\exception\ExceptionHandle;
use app\core\Request;
use app\core\Response;

return [
    'think\exception\Handle' => ExceptionHandle::class,
    'request' => Request::class,
    'response' => Response::class,
];
