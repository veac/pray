<?php

namespace app\core\exception;

/**
 * 业务异常类
 */
class BusinessException extends \RuntimeException
{
    protected $error;

    /**
     * @param $error
     * @param string $code
     */
    public function __construct($error, string $code = 'BUSINESS')
    {
        $this->error   = $error;
        $this->code = $code;
        $this->message = is_array($error) ? implode(PHP_EOL, $error) : $error;
    }

    /**
     * 获取验证错误信息
     * @access public
     * @return array|string
     */
    public function getError()
    {
        return $this->error;
    }

}