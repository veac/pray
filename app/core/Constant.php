<?php
namespace app\core;

class Constant {
    const STATUS_NORMAL = '0';
    const STATUS_DISABLED = '1';

    const MALE = '0';
    const FEMALE = '1';

    const DATA_SCOPE_ALL = '1';
    const DATA_SCOPE_DIV = '2';
    const DATA_SCOPE_DEPT = '3';
    const DATA_SCOPE_DEPTS = '4';
    const DATA_SCOPE_SELF = '5';

    const VISIBLE = '0';
    const INVISIBLE = '1';
    const IS_CACHE = 0;
    const NOT_CACHE = 1;
    const MENU_DIR = 0;
    const MENU_ROUTE = 1;
    const MENU_BUTTON = 2;
    const IS_FRAME = 0;
    const NO_FRAME = 1;

    const LOGIN_SUCCESS = 2;
    const LOGIN_FAIL = 1;
}

