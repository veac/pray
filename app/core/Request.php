<?php

namespace app\core;

class Request extends \think\Request
{
    /**
     * 登录用户
     * @var mixed
     */
    private $user = null;
    private $member = null;
    private $temple = null;

    /**
     * @param array $args
     * @param bool $newInstance
     * @return Request
     */
    public static function invoke(array $args = [], bool $newInstance = false): Request
    {
        return app(self::class, $args, $newInstance);
    }

    /**
     * @param string|null $col
     * @return mixed
     */
    public function getUser(string $col = null)
    {
        if (empty($col)) {
            return $this->user;
        } else {
            return $this->user[$col] ?? null;
        }
    }

    public function getMember(string $col = null)
    {
        if (empty($col)) {
            return $this->member;
        } else {
            return $this->member[$col] ?? null;
        }
    }

    public function getTemple(string $col = null)
    {
        if (empty($col)) {
            return $this->temple;
        } else {
            return $this->temple[$col] ?? null;
        }
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @param mixed $member
     */
    public function setMember($member): void
    {
        $this->member = $member;
    }

    /**
     * @param mixed $temple
     */
    public function setTemple($temple): void
    {
        $this->temple = $temple;
    }

    /**
     * 获取当前权限标识
     * @return string
     */
    public function getPerm(): string
    {
        return $this->rule->getName();
    }

    public function isSuperAdmin(): bool
    {
        $roles = $this->getUser();
        return in_array('admin', $roles) == true;
    }

}
