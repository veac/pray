<?php

namespace app\core;

/**
 * @template UtilType
 */
class Util
{

    /**
     * @return UtilType
     */
    public static function invoke(array $args = [], bool $newInstance = false)
    {
        return app(static::class, $args, $newInstance);
    }

}