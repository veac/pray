<?php
/**
 * Created by PhpStorm.
 * User: chenxiaomo
 * Date: 8/7/22
 * Time: 6:02 PM
 */

namespace app\core\util;;

use think\Exception;

/*
 *  Redis操作类
 *  单例+支持长连接 模式
 */

class Redis
{
    const REDISTIMEOUT = 0; //超时
    private static $_instance = []; //类单例数组
    private $hash;
    private $redis; //redis连接句柄

    private static $config = [
        "host" => '',
        "port" => '',
        "db" => 0,
        "auth" => '',
        "pconnect" => 1
    ];

    private function __construct($redis_config = [])
    {

        $this->redis = new \Redis();
        $this->hash = $redis_config["db"];

        if ($redis_config["pconnect"]) {
            $this->redis->pconnect($redis_config['host'], $redis_config['port'], self::REDISTIMEOUT);
        } else {
            $this->redis->connect($redis_config['host'], $redis_config['port'], self::REDISTIMEOUT);
        }
        //设置连接密码
        if ($redis_config["auth"]) {
            $this->redis->auth($redis_config["auth"]);
        }
        //选择库 0-15
        $this->redis->select($this->hash);
    }

    // 外部获取实例
    public static function getInstance($redis_config = [])
    {

        $redis_config = array_merge(self::$config, $redis_config);
        $key = $redis_config["host"]."|".$redis_config['db'];
        if (!isset(self::$_instance[$key])) {
            self::$_instance[$key] = new self($redis_config);
        }

        //防止挂掉
        try {
            self::$_instance[$key]->Ping() == 'Pong';
        } catch (Exception $e) {
            // 断线重连
            self::$_instance[$key] = new self($redis_config);
        }
        return self::$_instance[$key];
    }

    //获取redis的连接实例
    public function getRedisConnect()
    {
        return $this->redis;
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->redis, $method], $args);
    }


    /**
     * 关闭单例时做清理工作
     */
    public function __destruct()
    {
        $key = $this->hash;
        $this->redis->close();
        self::$_instance[$key] = null;
    }

    private function __clone()
    {
    }
}