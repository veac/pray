<?php

namespace app\core;

use think\App;

/**
 * @template ServiceType
 */
class Service
{
    /**
     * 应用实例
     * @var App
     */
    protected $app;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app, Request $request, $key = "65FE0E503A36", $method = 'AES-128-ECB', $iv = '', $options = 0)
    {
        $this->app = $app;
        $this->app->request = $request;

        // 控制器初始化
        $this->initialize();
        $this->secret_key = $key;
        $this->method = $method;
        $this->iv = $iv;
        $this->options = $options;
        $this->char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $this->userName = "ZZLWECE";
        $this->password = "MDaCGAa8v1@U";
        $this->key = "LnD2ZGb1@pFG";
        $this->appCode = "ZZSYQT";
        $this->neCode = "ZZSYQT";
    }

    protected $appCode;
    protected $neCode;
    protected $char;
    protected $key;
    protected $userName;
    protected $password;
    protected $method;
    protected $secret_key;
    protected $iv;

    // 初始化
    protected function initialize()
    {
    }

    /**
     * @return ServiceType
     */
    public static function invoke(array $args = [], bool $newInstance = false)
    {
        return app(static::class, $args, $newInstance);
    }

}