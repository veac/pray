<?php
declare (strict_types=1);

namespace app\core;

use app\model\SysUser;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use stdClass;
use think\App;

/**
 * 控制器基础类
 */
abstract class ApiController
{
    /**
     * Request实例
     * @var Request
     */
    protected $request;

    /**
     * Response实例
     * @var Response
     */
    protected $response;

    /**
     * 应用实例
     * @var App
     */
    protected $app;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app, Request $request, Response $response, $key = "65FE0E503A36", $method = 'AES-128-ECB', $iv = '', $options = 0)
    {
        $this->app = $app;
        $this->app->request = $request;
        $this->request = $request;
        $this->response = $response;

        // 控制器初始化
        $this->initialize();
        $this->secret_key = $key;
        $this->method = $method;
        $this->iv = $iv;
        $this->options = $options;
        $this->char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $this->userName = "ZZLWECE";
        $this->password = "MDaCGAa8v1@U";
        $this->key = "LnD2ZGb1@pFG";
        $this->appCode = "ZZSYQT";
        $this->neCode = "ZZSYQT";
    }

    protected $appCode;
    protected $neCode;
    protected $char;
    protected $key;
    protected $userName;
    protected $password;
    protected $method;

    protected $secret_key;
    protected $iv;

    // 初始化
    protected function initialize()
    {
    }
    

}
