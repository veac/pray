<?php
/**
 * Created by PhpStorm.
 * User: kingwang
 * Date: 2024/6/28
 * Time: 15:31
 */

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\BankService;
use app\service\TempleBankService;


class TempleBank extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery', null);
        $searchQuery['type'] = 3;//1-支付宝 2-微信 3-银行卡
        $data = TempleBankService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($data);
    }

    public function getBank()
    {
        $data = BankService::invoke()->getBank();
        $this->response->success($data);
    }

    public function save()
    {
        $temple_id = Request::invoke()->getTemple('temple_id');
        $data = input('post.');
        $id = input('post.id');
        $data['temple_id'] = $temple_id;
        TempleBankService::invoke()->save($data, $id);
        $this->response->success([], '成功');
    }

    public function info()
    {
        $temple_id = Request::invoke()->getTemple('temple_id');
        $type = input('get.type', 1);
        $id = 0;
        if ($type == 1) {
            $id = input('get.id', 0);
        }
        $data = TempleBankService::invoke()->info($id, $temple_id, $type);
        $this->response->success($data);
    }

}