<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\IncomeService;

class Income extends ApiController
{
    public function list()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $list = IncomeService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }
}