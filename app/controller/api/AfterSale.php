<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\ProductOrderService;


class AfterSale extends ApiController
{
    /**
     * 售后申请
     */
    public function apply()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('post.order_id');
        $data = input('post.', []);
        $data = ProductOrderService::invoke()->afterSale($order_id, $member_id, $data);
        $this->response->success($data);
    }

    public function getReason()
    {
        $data = [
            ['name' => '不喜欢、效果不好'],
            ['name' => '不想要了'],
            ['name' => '缺少功能'],
            ['name' => '性能故障'],
            ['name' => '外观、型号、参数与描述不符'],
            ['name' => '货物与描述不符'],
            ['name' => '质量问题'],
            ['name' => '收到商品少件(含少配件)'],
            ['name' => '商品破损或污渍'],
            ['name' => '商家发错货'],
            ['name' => '假冒品牌'],
            ['name' => '空包裹'],
            ['name' => '其他原因']
        ];
        $this->response->success($data);
    }

}