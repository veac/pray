<?php


namespace app\controller\api;


use app\core\ApiController;
use app\core\Request;
use app\service\BlessingOrderService;
use app\service\ProductOrderService;

class WeChatPay extends ApiController
{
    /**
     *
     */
    public function index()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $memberInfo = Request::invoke()->getMember();
        if (!$memberInfo['js_openid']) {
            $this->response->error('openid无数据');
        }
        $order_id = input('post.order_id');
        $order_type = input('post.order_type', 1);
        if ($order_type == 3) {
            $info = ProductOrderService::invoke()->info($order_id, $member_id);//小程序商品
        } else {
            $info = BlessingOrderService::invoke()->info($order_id, $member_id);//祈福供灯订单
        }
        if (!$info) {
            $this->response->error('订单不存在');
        }
        // 当前时间
        $time = time();
        $openid = $memberInfo['js_openid'];
        $appid = $this->app->config->get('app.js_app_id');
        $mch_id = $this->app->config->get('app.mch_id');
        $apikey = $this->app->config->get('app.app_key');
        // 生成随机字符串
        $nonceStr = md5($time . $openid);
        // API参数
        $params = [
            'appid' => $appid,
            'attach' => json_encode(['order_type' => $info['order_type']]),
            'body' => '祈福订单',
            'mch_id' => $mch_id,
            'nonce_str' => $nonceStr,
            'notify_url' => 'https://pray.veac.cn/api/notify/notice',  // 异步通知地址
            'openid' => $openid,
            'out_trade_no' => $info['order_no'],
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'], //终端 IP
            'total_fee' => 1, // 价格:单位分
            'trade_type' => 'JSAPI',
        ];
        // 生成签名
        $params['sign'] = $this->makeSign($params, $apikey);
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->post($url, $this->toXml($params));
        $prepay = $this->xmlToArray($result);
        if ($prepay['return_code'] === 'FAIL') {
            $errMsg = "微信支付api：{$prepay['return_msg']}";
            $this->response->error($errMsg);
        }
        if ($prepay['result_code'] === 'FAIL') {
            $errMsg = "微信支付api：{$prepay['return_msg']}";
            $this->response->error($errMsg);
        }
        // 生成 nonce_str 供前端使用
        $paySign = $this->makePaySign($params['nonce_str'], $prepay['prepay_id'], $time, $apikey, $appid);
        $data = [
            'package' => 'prepay_id=' . $prepay['prepay_id'],
            'nonceStr' => $nonceStr,
            'timeStamp' => (string)$time,
            'paySign' => $paySign,
            'signType' => 'MD5',
            'appId' => $appid,
        ];
        $this->response->success($data);
    }

    /**
     * 生成paySign
     * @param $nonceStr
     * @param $prepay_id
     * @param $timeStamp
     * @return string
     */
    private function makePaySign($nonceStr, $prepay_id, $timeStamp, $apikey, $appId)
    {
        $data = [
            'appId' => $appId,
            'nonceStr' => $nonceStr,
            'package' => 'prepay_id=' . $prepay_id,
            'signType' => 'MD5',
            'timeStamp' => $timeStamp,
        ];
        // 签名步骤一：按字典序排序参数
        ksort($data);
        $string = $this->toUrlParams($data);
        // 签名步骤二：在string后加入KEY
        $string = $string . '&key=' . $apikey;
        // 签名步骤三：MD5加密
        $string = md5($string);
        // 签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 输出xml字符
     * @param $values
     * @return bool|string
     */
    private function toXml($values)
    {
        if (!is_array($values)
            || count($values) <= 0
        ) {
            return false;
        }

        $xml = "<xml>";
        foreach ($values as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    protected function post(string $url, $data = [], $useCert = false, $sslCert = [])
    {
        $header = [
            'Content-type: application/json;'
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        if ($useCert == true) {
            // 设置证书：cert 与 key 分别属于两个.pem文件
            curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');
            curl_setopt($curl, CURLOPT_SSLCERT, $sslCert['certPem']);
            curl_setopt($curl, CURLOPT_SSLKEYTYPE, 'PEM');
            curl_setopt($curl, CURLOPT_SSLKEY, $sslCert['keyPem']);
        }
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    private function makeSign($values, $apikey)
    {
        //签名步骤一：按字典序排序参数
        ksort($values);
        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . '&key=' . $apikey;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 格式化参数格式化成url参数
     * @param $values
     * @return string
     */
    private function toUrlParams($values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }

    /**
     * 转换成数组
     */
    private function xmlToArray($xml)
    {
        $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $val = json_decode(json_encode($xmlstring), true);
        return $val;
    }


}