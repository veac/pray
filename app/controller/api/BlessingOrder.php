<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\BlessingOrderService;

class BlessingOrder extends ApiController
{
    /**
     *祈福供灯订单
     */
    public function create()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $data = input('post.');
        $rs = BlessingOrderService::invoke()->save($data, $member_id);
        $this->response->success($rs, '成功');
    }

    public function list()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $list = BlessingOrderService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *订单详情接口
     */
    public function info()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('get.order_id');
        $data = BlessingOrderService::invoke()->info($order_id, $member_id);
        $this->response->success($data);
    }

    /**
     *取消订单接口
     */
    public function cancel()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('post.order_id');
        BlessingOrderService::invoke()->cancelOrder($order_id, $member_id);
        $this->response->success([]);
    }

    /**
     *删除订单接口
     */
    public function del()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('post.order_id');
        BlessingOrderService::invoke()->delOrder($order_id, $member_id);
        $this->response->success([]);
    }

    /**
     *删除订单接口-定时任务
     */
    public function orderTask()
    {
        $list = (new \app\model\BlessingOrder())->where(['status' => 1])->order('order_id desc')->select();
        if (!empty($list)) {
            $list = $list->toArray();
            foreach ($list as $map) {
                $time = time() - 180;
                if (strtotime($map['create_time']) < $time) {
                    BlessingOrderService::invoke()->cancelOrder($map['order_id'], $map['member_id']);
                }
            }
        }
        $this->response->success([]);
    }
}