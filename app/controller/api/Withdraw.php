<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\TempleService;
use app\service\WithdrawService;

class Withdraw extends ApiController
{
    /**
     * 获取提现明细
     */
    public function list()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $list = WithdrawService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *提现接口
     */
    public function create()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $data = input('post.');
        $rs = WithdrawService::invoke()->save($data, $member_id);
        $this->response->success($rs);
    }

}