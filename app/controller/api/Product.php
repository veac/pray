<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\CompanyService;
use app\service\ProductService;

class Product extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = ProductService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = ProductService::invoke()->info($id);
        $this->response->success($info);
    }
}