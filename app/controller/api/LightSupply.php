<?php

namespace app\controller\api;

use app\core\ApiController;
use app\service\LightSupplyService;
use app\service\CateService;

class LightSupply extends ApiController
{
    /**
     * 祈福心愿列表
     */
    public function list()
    {
        $searchQuery = input('get.', []);
        $list = LightSupplyService::invoke()->getList(1, 20, $searchQuery);
        $this->response->success($list);
    }

    public function cateList()
    {
        $searchQuery = input('get.', []);
        $list = CateService::invoke()->list($searchQuery);
        $this->response->success($list);
    }


}