<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\BlessingOrderService;
use app\service\ProductOrderService;

class ProductOrder extends ApiController
{
    /**
     *认购订单
     */
    public function create()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $data = input('post.');
        $rs = ProductOrderService::invoke()->save($data, $member_id);
        $this->response->success($rs, '成功');
    }

    public function list()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $list = ProductOrderService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *订单详情接口
     */
    public function info()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('get.order_id');
        $data = ProductOrderService::invoke()->info($order_id, $member_id);
        $this->response->success($data);
    }
    /**
     *取消订单接口
     */
    public function cancel()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('post.order_id');
        ProductOrderService::invoke()->cancelOrder($order_id, $member_id);
        $this->response->success([]);
    }

    /**
     *删除订单接口
     */
    public function del()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('post.order_id');
        ProductOrderService::invoke()->delOrder($order_id, $member_id);
        $this->response->success([]);
    }

}