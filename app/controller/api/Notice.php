<?php

namespace app\controller\api;

use app\core\ApiController;
use app\service\NoticeService;

class Notice extends ApiController
{
    /**
     * 公告列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery['status'] = 1;
        $list = NoticeService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

}