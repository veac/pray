<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\AddressService;

class Address extends ApiController
{
    public function list()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', null);
        $searchQuery['member_id'] = $member_id;
        $data = AddressService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($data);
    }

    public function save()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $data = input('post.');
        $address_id = input('post.address_id');
        $data['member_id'] = $member_id;
        AddressService::invoke()->save($data, $address_id);
        $this->response->success([], '成功');
    }

    public function info()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $id = input('get.id', 0);
        $is_default = input('get.is_default', 0);
        $data = AddressService::invoke()->info($id, $member_id, $is_default);
        $this->response->success($data);
    }

    public function del()
    {
        $id = input('post.id');
        AddressService::invoke()->del($id);
        $this->response->success();
    }
}