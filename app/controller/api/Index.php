<?php

namespace app\controller\api;

use app\core\ApiController;
use app\model\Gdp;
use app\model\HousePrice;
use app\service\BannerService;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;

class Index extends ApiController
{
    public function mini()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $token = 'feKDL3334dfff565cvhfj6h5555';
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        echo htmlspecialchars($_GET["echostr"]);exit();
        if ($signature == $tmpStr) {
            echo htmlspecialchars($_GET["echostr"]);
        } else {
            //反之不进行返回参数
            exit;
        }
    }

    /**
     *minisystem
     */
    public function minisystem()
    {
        $data = [
            'url' => 'https://pray.veac.cn/storage/20240525/fa7891347c7fbde7ae62d8b4904b7983.png',
            'title' => '佛前供花',
            'sub_title' => '相貌端庄为何因 前世鲜花供佛前',
        ];
        $this->response->success($data);
    }

    /**
     * 首页banner接口
     */
    public function banner()
    {
        $page = input('get.page', 1);
        $limit = input('get.limit', 10);
        $searchQuery = input('get.', []);
        $searchQuery['type'] = input('get.type', 1);
        $list = BannerService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }



    public function sug()
    {
        $page = 1;
        $length = 300;
        $offset = $length * ($page - 1);
        $HousePriceList = (new HousePrice())->where([])->limit($offset, $length)->select()->toArray();
        $itemId = 0;
        foreach ($HousePriceList as $value) {
            $query = urlencode($value['poi_name']);
            $url = 'https://map.ke.com/proxyApi/i.c-pc-webapi.ke.com/map/sug?cityId=360400&dataSource=ESF&query=' . $query;
            $rs = $this->http_get($url);
            $rs = json_decode($rs, true);
            if (!empty($rs['data']['list'])) {
                $list = $rs['data']['list'];
                foreach ($list as $val) {
                    if ($val['itemType'] == 'resblock') {
                        $itemId = $val['itemId'];
                    }
                }
                if (empty($itemId)) {
                    continue;
                }
                $res = $this->houselist($itemId);
                $res = json_decode($res, true);
                if (!empty($res['data']['resblockCard'])) {
                    $resblockCard = $res['data']['resblockCard'];
                    (new HousePrice())->where(['id' => $value['id']])->save(['price' => $resblockCard['avgUnitPrice']]);
                    var_dump('++==' . $value['poi_name'] . '++==');
                }
            } else {
                var_dump($value['poi_name']);
            }
        }
    }

    public function houselist($resblockId)
    {
        $url = 'https://map.ke.com/proxyApi/i.c-pc-webapi.ke.com/map/houselist';
        $params = [
            'cityId' => 360400,
            'dataSource' => 'ESF',
            'curPage' => 1,
            'type' => 'resblock',
            'resblockId' => $resblockId,
        ];
        $SignStr = $this->getSignStr($params);
        $url .= '?' . $SignStr;
        $rs = $this->httpGet($url);
        return $rs;
    }

    public function getSignStr($params)
    {
        if (!ksort($params)) {
            printf("make_signed_array_utf8::sort arr_input failed, arr: %s", printf($params, true));
            return false;
        }
// 构造签名串
        $signStr = '';
        foreach ($params as $inputKey => $inputVal) {
            $signStr .= '&' . $inputKey . '=' . $inputVal;
        }
        $signStr = substr($signStr, 1);
        return $signStr;
    }

    public function http_get($url, $header = array())
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "cache-control: no-cache"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function httpGet($url, $header = array())
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }


    public function getGdpAndPeople()
    {
        $timestamp = input('post.timestamp', '');
        $signature = input('post.signature', '');
        $area = input('post.area', '');
        $sale = 'e0u6fnlag06lc3p3';
        $mdSign = md5($area . $timestamp . $sale);
        if ($signature != $mdSign) {
            $this->response->error('签名错误');
        }
        $city = input('post.city', '');
        $w = [];
        if ($area) {
            $w['area'] = $area;
        }
        if ($city) {
            $w['city'] = $city;
        }
        if (empty($w)) {
            return false;
        }
        $gdp_sum = $people_sum = 0;
        $gdpList = (new Gdp())->where($w)->select()->toArray();
        if ($gdpList) {
            $people = $gdp = [];
            foreach ($gdpList as $value) {
                $gdp[] = $value['gdp'];
                $people[] = $value['people'];
            }
            $gdp_sum = array_sum($gdp);
            $people_sum = array_sum($people);
        }
        $data = [
            'gdp' => $gdp_sum,
            'people' => $people_sum,
        ];
        $this->response->success($data, '成功');
    }

    public function getGdp()
    {
        $timestamp = input('post.timestamp', '');
        $signature = input('post.signature', '');
        $area = input('post.area', '');
        $city = input('post.city', '');
        $years = input('post.years', 0);
        $sale = 'e0u6fnl23fag03lc3p3';
        $mdSign = md5($city . $years . $timestamp . $sale);
        if ($signature != $mdSign) {
            $this->response->error('签名错误');
        }
        $w = [];
        if ($area) {
            $w['area'] = $area;
        }
        if ($city) {
            $w['city'] = $city;
        }
        if ($years) {
            $w['years'] = $years;
        }
        if (empty($w)) {
            return false;
        }
        $gdp_sum = 0;
        $gdpList = (new Gdp())->where($w)->select()->toArray();
        if ($gdpList) {
            $gdp = [];
            foreach ($gdpList as $value) {
                $gdp[] = $value['gdp'];
            }
            $gdp_sum = array_sum($gdp);
        }
        $data = [
            'gdp' => $gdp_sum,
        ];
        if ($gdp_sum > 0) {
            if ($city) {
                $data['city'] = $city;
            }
            if ($area) {
                $data['area'] = $area;
            }
            if ($years) {
                $data['years'] = $years;
            }
        }
        $this->response->success($data, '成功');
    }

    public function getHousePrice()
    {
        $timestamp = input('post.timestamp', '');
        $signature = input('post.signature', '');
        $poi_name = input('post.poi_name', '');
        $sale = 'e0u6fnlag06lc3p3';
        $mdSign = md5($poi_name . $timestamp . $sale);
        if ($signature != $mdSign) {
            $this->response->error('签名错误');
        }
        $w = [];
        if ($poi_name) {
            $w['poi_name'] = $poi_name;
            $w[] = ['poi_name', 'like', "%" . $poi_name . "%"];
        }
        if (empty($w)) {
            return false;
        }
        $House = (new HousePrice())->where($w)->find();
        if (empty($House)) {
            return false;
        }
        $House = $House->toArray();
        unset($House['id']);
        $data = $House;
        $this->response->success($data, '成功');
    }

}