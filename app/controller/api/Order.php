<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\IncomeService;
use app\service\OrderService;

class Order extends ApiController
{
    /**
     *小程序商品订单
     */
    public function create()
    {
        $type = rand(1, 3);
        if ($type == 1) {
            $member_id = Request::invoke()->getMember('member_id');
            $data = input('post.');
            $rs = OrderService::invoke()->save($data, $member_id);
            $this->response->success(['type' => $type, 'info' => $rs], '认购成功');
        } elseif ($type == 2) {
            $this->response->success(['type' => $type], '认购作废');
        } elseif ($type == 3) {
            $this->response->success(['type' => $type], '认购失败');
        }
    }

    /**
     *我的认购列表
     */
    public function myList()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $searchQuery['status'] = 2;
        $list = OrderService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *认购列表-我的团队
     */
    public function myTeamList()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $searchQuery['status'] = 2;
        $list = OrderService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *认购详情明细接口
     */
    public function detail()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $order_id = input('get.order_id');
        $data = OrderService::invoke()->getDetail($order_id, $member_id);
        $this->response->success($data);
    }

    /**
     *认购详情明细接口
     */
    public function incomeList()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('get.page', 1);
        $limit = input('get.limit', 10);
        $searchQuery = input('get.', []);
        $searchQuery['member_id'] = $member_id;
        $list = IncomeService::invoke()->getIncomeListByOrder($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     *我的-分销订单
     */
    public function distribution()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['member_id'] = $member_id;
        $data = OrderService::invoke()->getDistribution($page, $limit, $searchQuery);
        $this->response->success($data);
    }


}