<?php

namespace app\controller\api;

use app\core\ApiController;
use app\core\Request;
use app\service\BankService;
use app\service\IncomeService;
use app\service\MemberService;
use app\service\OrderService;
use app\model\Order;
use \app\model\Member;
use app\service\TeamIncomeService;

class Bank extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery', null);
        $searchQuery['type'] = 3;//1-支付宝 2-微信 3-银行卡
        $data = BankService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($data);
    }

    public function getBank()
    {
        $data = BankService::invoke()->getBank();
        $this->response->success($data);
    }

    public function save()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $data = input('post.');
        $id = input('post.id');
        $data['member_id'] = $member_id;
        BankService::invoke()->save($data, $id);
        $this->response->success([], '成功');
    }

    public function info()
    {
        $member_id = Request::invoke()->getMember('member_id');
        $type = input('get.type', 1);
        $id = 0;
        if ($type == 1) {
            $id = input('get.id', 0);
        }
        $data = BankService::invoke()->info($id, $member_id, $type);
        $this->response->success($data);
    }

}