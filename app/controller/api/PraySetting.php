<?php

namespace app\controller\api;

use app\core\ApiController;
use app\service\PraySettingService;
use app\service\TempleSettingService;

class PraySetting extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 30);
        $searchQuery = input('post.', []);
        if (!empty($searchQuery['temple_id'])) {
            $list = TempleSettingService::invoke()->list($page, $limit, $searchQuery);
        }
        if (empty($list)) {
            $list = PraySettingService::invoke()->list($page, $limit);
        }
        $this->response->success($list);
    }
}