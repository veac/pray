<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\SettingsService;

class Settings extends ApiController
{
    /**
     * 设置-联系我们-登录图片
     */
    public function info()
    {
        $type = input('get.type', 0);
        $info = SettingsService::invoke()->getInfo($type);
        $this->response->success($info);
    }

    public function save()
    {
        $data = input('post.', []);
        $id = input('post.id', 0);
        $rs = SettingsService::invoke()->save($data, $id);
        $this->response->success($rs);
    }

    public function setCharge()
    {
        $data = input('post.', []);
        $type = 2;//手续费 百分比 1为1%
        $rs = SettingsService::invoke()->saveCharge($data, $type);
        $this->response->success($rs);
    }

    public function getCharge()
    {
        $type = 2;
        $info = SettingsService::invoke()->getInfo($type);
        $data =[
            'id'=>$info['id'],
            'charge'=>$info['charge'],
        ];
        $this->response->success($data);
    }
}