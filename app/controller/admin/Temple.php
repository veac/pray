<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\QrCodeService;
use app\service\TempleImageService;
use app\service\TempleService;

class Temple extends ApiController
{

    /**
     * 获取寺庙列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = TempleService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 保存寺庙信息
     */
    public function save()
    {
        $data = input('post.');
        $id = input('post.temple_id', 0);
        $searchQuery['temple_status'] = 2;
        $rs = TempleService::invoke()->save($data, $id);
        $this->response->success($rs);
    }

    public function imageList()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = TempleImageService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 保存图片
     */
    public function saveImage()
    {
        $data = input('post.');
        $rs = TempleImageService::invoke()->save($data);
        $this->response->success($rs);
    }

    public function delImage()
    {
        $id = input('post.id');
        TempleImageService::invoke()->del($id);
        $this->response->success();
    }

    /**
     * 获取详情
     */
    public function info()
    {
        $id = input('get.id');
        $info = TempleService::invoke()->info($id);
        $this->response->success($info);
    }

    /**
     * 删除寺庙
     */
    public function del()
    {
        $id = input('post.id');
        TempleService::invoke()->del($id);
        $this->response->success();
    }

    /**
     * 获取二维码图片链接
     */
    public function getQrcode()
    {
        $id = input('get.id', 0);
        $url = 'https://app.fjxmlz.com/htmls/#/pages/templelist/templedetail?id=' . $id;
        $rs = QrCodeService::invoke()->getCode($url);
        $this->response->success($rs);
    }

    /**
     * 批量更新新增
     */
    public function updateAll()
    {
        $rs = TempleService::invoke()->updateAll();
        $this->response->success($rs);
    }
}