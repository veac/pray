<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\LampApplyService;

class LampApply extends ApiController
{
    /**
     * 公德灯申请列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = LampApplyService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

}