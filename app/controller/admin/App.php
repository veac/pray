<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\AppServiec;

class App extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery', null);

        $data = AppServiec::invoke()->list($page, $limit, $searchQuery);

        $this->response->success($data);
    }

    public function del()
    {
        $app_id = input('post.app_id');
        AppServiec::invoke()->del($app_id);
        $this->response->success(true);
    }

    public function create()
    {
        $data = input('post.');
        AppServiec::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $app_id = input('post.app_id');
        AppServiec::invoke()->save($data, $app_id);
        $this->response->success(true);
    }

    public function info()
    {
        $app_id = input('get.app_id');
        $data = AppServiec::invoke()->info($app_id);
        $this->response->success($data);
    }

    public function select()
    {
        $res = AppServiec::invoke()->select();
        $this->response->success($res);
    }
}