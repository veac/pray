<?php

namespace app\controller\admin;

use app\core\ApiController;

use app\service\BlessingOrderService;
use app\service\TeamIncomeService;

class Index extends ApiController
{

    public function index()
    {
        $searchQuery = input('get.', []);
        $year = input('get.year', '');
        $commission = TeamIncomeService::invoke()->sumCommission();
        $temple_num = (new \app\model\Temple())::where(['is_delete' => 0])->count();
        $lamp_num = (new \app\model\LotusLamp())::where(['is_delete' => 0])->count();
        $list = [];
        for ($i = 1; $i <= 12; $i++) {
            $start = date('Y-m-d H:i:s', strtotime($year . '-' . $i . '-1'));
            $a = $i + 1;
            $end = date('Y-m-d H:i:s', strtotime($year . '-' . $a . '-1') - 1);
            $searchQuery['create_time'] = $start . '#' . $end;
            $commissions = TeamIncomeService::invoke()->sumCommission($searchQuery);
            $price = BlessingOrderService::invoke()->sumPrice($searchQuery);
            $list[] = [
                'month' => $year . '-' . sprintf("%02d", $i),
                'commissions' => $commissions ?: 0.00,
                'price' => $price ?: 0.00,
            ];
        }
        $data = [
            'commission' => $commission ?: 0.00,
            'temple_num' => $temple_num ?: 0,
            'lamp_num' => $lamp_num ?: 0,
            'agent_num' => 0,
            'sales_num' => 0,
            'apply_order' => 0,
            'weibao_order' => 0,
            'weihu_order' => 0,
            'list' => $list,
        ];
        $this->response->success($data);
    }

    /**
     * @return App
     */
    public function getAddress()
    {
        $address = $searchQuery = input('post.address', '');
        $url = 'https://restapi.amap.com/v3/geocode/geo?address=' . $address . '&output=JSON&key=d2087318d232db163c502b8448172f6c';
        $data = $this->httpGet($url);
        $data = json_decode($data, true);
        $this->response->success($data);
    }

    public function httpGet($url, $header = array())
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }


}