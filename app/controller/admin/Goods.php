<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\CompanyService;
use app\service\GoodsService;
use app\service\ProductService;

class Goods extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['status'] = input('post.status', 10);
        $searchQuery['is_delete'] = 0;
        $list = GoodsService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $goods_id = input('get.goods_id');
        $info = GoodsService::invoke()->info($goods_id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        GoodsService::invoke()->save($data);
        $this->response->success(true);
    }

    public function del()
    {
        $id = input('post.goods_id');
        GoodsService::invoke()->del($id);
        $this->response->success();
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.goods_id');
        GoodsService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function saveStatus()
    {
        $status = input('post.status');
        $id = input('post.goods_id');
        GoodsService::invoke()->saveStatus($status, $id);
        $this->response->success(true);
    }
}