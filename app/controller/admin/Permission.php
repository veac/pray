<?php

namespace app\controller\admin;


use app\core\ApiController;
use app\service\PermissionService;

/**
 * 权限管理
 */
class Permission extends ApiController
{
    public function list()
    {
        $appKey = input('post.app_key');
        $searchQuery = input('post.searchQuery/a', null);
        $list = PermissionService::invoke()->getSysPermissionList($appKey, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = PermissionService::invoke()->getPermissionInfo($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        PermissionService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.permission_id');
        PermissionService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function tree()
    {
        $appKey = input('get.app_key');
        $tree = PermissionService::invoke()->getPermissionTree($appKey);
        $this->response->success($tree);
    }

    public function del()
    {
        $id = input('post.id');
        PermissionService::invoke()->delById($id);
        $this->response->success(true);
    }
}