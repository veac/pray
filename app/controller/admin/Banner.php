<?php


namespace app\controller\admin;


use app\core\ApiController;
use app\service\BannerService;

class Banner extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['type'] = input('post.type', 1);
        $list = BannerService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function save()
    {
        $data = input('post.');
        $id = input('post.id');
        BannerService::invoke()->save($data, $id);
        $this->response->success([], '成功');
    }

    public function del()
    {
        $id = input('post.id');
        BannerService::invoke()->del($id);
        $this->response->success();
    }
}