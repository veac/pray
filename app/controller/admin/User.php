<?php

namespace app\controller\admin;


use app\core\ApiController;
use app\core\Constant;
use app\core\Request;
use app\core\util\Encryp;
use app\model\CompanyHelp;
use app\model\SysRole;
use app\model\SysUser;
use app\model\ZtcUser;
use app\service\MenuService;
use app\service\UserService;
use app\service\ZqztcService;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

class User extends ApiController
{
    public function login()
    {
        $username = input('post.user_name');
        $password = input('post.password');
        $userSerice = UserService::invoke();
        $userInfo = SysUser::where([
            'user_name' => $username])->find()->toArray();
        if (empty($userInfo)) {
            $this->response->error('本系统查无此帐号，请告知工作人员添加！');
            return;
        }
        $token = $userSerice->login($username, $password);
        $this->response->success(['token' => $token], "登录成功");
    }

    public function bindWx()
    {
        $userId = Request::invoke()->getUser('user_id');
        $userInfo = UserService::invoke()->getUserInfo($userId);
        $openId = data_get($userInfo, 'open_id');
        if ($openId) {
            $this->response->error('账户已绑定微信');
            return;
        }
        $code = input('code');
        if (empty($code)) {
            $this->response->error('code 参数必传');
            return;
        }
        $userId = Request::invoke()->getUser('user_id');
        $msg = UserService::invoke()->bindWxUser($code, $userId);
        if ($msg) {
            $this->response->error($msg);
            return;
        }
        $this->response->success();
    }

    /**
     * 获取用户信息
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getUserInfo()
    {
        $userSerice = UserService::invoke();
        $userInfo = $userSerice->refreshUserInfo($this->request->getUser('user_id'));
        $this->response->success($userSerice->toSafe($userInfo));
    }

    /**
     * 用户信息详情
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function info()
    {
        $userId = input('get.user_id');
        $userService = UserService::invoke();
        $userInfo = $userService->getUserInfo($userId);
        $this->response->success($userService->toSafe($userInfo));
    }

    /**
     * 获取系统菜单
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function menuList()
    {
        $meunService = MenuService::invoke();
        $this->response->success($meunService->getMenus('ADMIN'));
    }

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery/a', null);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = UserService::invoke()->getUserList($page, $limit, $searchQuery);
        $sysRole = SysRole::field("role_id, role_name")
            ->where('status', Constant::STATUS_NORMAL)
            ->select()->toArray();
        $sysRole = array_column($sysRole, null, 'role_id');
        foreach ($list['list'] as &$value) {
            $value['role_name'] = $value['role_id'] ? $sysRole[$value['role_id']]['role_name'] : '';
            if (!$value['company_name']) {
                $value['company_name'] = $value['dept_name'];
            }
        }
        $this->response->success($list);
    }

    public function create()
    {
        $data = input('post.');
        UserService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $user_id = input('post.user_id');
        UserService::invoke()->save($data, $user_id);
        $this->response->success(true);
    }

    public function del()
    {
        $userId = input('post.user_id');
        UserService::invoke()->del($userId);
        $this->response->success(true);
    }

    public function select()
    {
        $type = input('post.type', 'list');
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery/a', null);
        $user_ids = input('post.user_ids', []);
        $list = UserService::invoke()->select($type, $page, $limit, $searchQuery, $user_ids);
        $this->response->success($list);
    }

    public function refreshAccToken()
    {
        $code = input('get.code');
        if ($code != 'r6QaeAbG66') {
            $this->response->error('code 错误');
        }
        $res = UserService::invoke()->getAccessToken();
        Log::write("refreshAccToken:$res");
        if ($res) {
            $this->response->success();
        } else {
            $this->response->error();
        }
    }
}