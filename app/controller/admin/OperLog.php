<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\OperLogService;

class OperLog extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery');

        $oper_log_service = OperLogService::invoke();

        $this->response->success($oper_log_service->list($page, $limit, $searchQuery));
    }

    public function info()
    {
        $oper_id = input('get.oper_id');
        $oper_log_service = OperLogService::invoke();
        $this->response->success($oper_log_service->info($oper_id));
    }
}