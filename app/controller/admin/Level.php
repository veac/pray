<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\LevelService;

class Level extends ApiController
{
    /**
     * 等级权益
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = LevelService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

}