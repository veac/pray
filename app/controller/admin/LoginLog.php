<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\LoginLogService;

class LoginLog extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery');

        $login_log_service = LoginLogService::invoke();

        $this->response->success($login_log_service->list($page, $limit, $searchQuery));
    }

    public function info()
    {
        $login_log_id = input('get.login_log_id');
        $login_log_service = LoginLogService::invoke();
        $this->response->success($login_log_service->info($login_log_id));
    }
}