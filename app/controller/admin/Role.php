<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\RoleService;

class Role extends ApiController
{
    public function select()
    {
        $res = RoleService::invoke()->selectRoles();
        $this->response->success($res);
    }

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery', null);
        $res = RoleService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($res);
    }

    public function create()
    {
        $data = input('post.');
        RoleService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $role_id = input('post.role_id', null);
        RoleService::invoke()->save($data, $role_id);
        $this->response->success(true);
    }

    public function info()
    {
        $role_id = input('get.role_id');
        $data = RoleService::invoke()->info($role_id);
        $this->response->success($data);
    }

    public function del()
    {
        $role_id = input('post.role_id');
        RoleService::invoke()->del($role_id);
        $this->response->success(true);
    }
}