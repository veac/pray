<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\AppealService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Appeal extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery/a', null);
        $list = AppealService::invoke()->getCompanyList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = AppealService::invoke()->info($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        AppealService::invoke()->save($data, Request::invoke()->getUser('user_id'));
        $this->response->success(true);
    }

    public function del()
    {
        $id = input('post.id');
        AppealService::invoke()->del($id);
        $this->response->success();
    }

    public function update()
    {
        $data = input('post.');
        $company_id = input('post.company_id');
        AppealService::invoke()->save($data, Request::invoke()->getUser('user_id'), $company_id);
        $this->response->success(true);
    }

    public function select()
    {
        $user_id = Request::invoke()->getUser('user_id');
        $data = AppealService::invoke()->select($user_id);
        $this->response->success($data);
    }

    /*
     * 导出数据
     */
    public function downloadExcel($name, $data = [], $head = [], $keys = [])
    {
        $count = count($head);  //计算表头数量
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        for ($i = 65; $i < $count + 65; $i++) {     //数字转字母从65开始，循环设置表头：
            $sheet->setCellValue(strtoupper(chr($i)) . '1', $head[$i - 65]);
        }
        //循环设置单元格：
        foreach ($data as $key => $item) {
            //$key+2,因为第一行是表头，所以写到表格时   从第二行开始写
            for ($i = 65; $i < $count + 65; $i++) {
                //数字转字母从65开始：
                $sheet->setCellValue(strtoupper(chr($i)) . ($key + 2), $item[$keys[$i - 65]]);
                //固定列宽
                $spreadsheet->getActiveSheet()->getColumnDimension(strtoupper(chr($i)))->setWidth(20);
            }
        }
        $names = $name;
        //utf-8转unicode格式
        $name = iconv('UTF-8', 'UCS-2BE', $name);
        $len = strlen($name);
        $str = '';
        for ($i = 0; $i < $len - 1; $i = $i + 2) {
            $c = $name[$i];
            $c2 = $name[$i + 1];
            if (ord($c) > 0) {
                $str .= '\u' . base_convert(ord($c), 10, 16) . str_pad(base_convert(ord($c2), 10, 16), 2, 0, STR_PAD_LEFT);
            } else {
                $str .= '\u' . str_pad(base_convert(ord($c2), 10, 16), 4, 0, STR_PAD_LEFT);
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Data-Type: binary');
        header('Data-Filename: ' . $str);
        header('Content-Disposition: attachment;filename="' . $names . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Access-Control-Expose-Headers:Data-Type,Data-Filename');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}