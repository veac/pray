<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\LampService;
use app\service\LotusLampService;
use app\service\MonitorService;
use app\service\QrCodeService;

class LotusLamp extends ApiController
{
    /**
     * 莲花灯列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = LotusLampService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = LotusLampService::invoke()->info($id);
        $this->response->success($info);
    }


    public function getList()
    {
        $page = input('get.page', 1);
        $limit = input('get.limit', 50);
        $searchQuery['temple_status'] = 2;
        $list = LampService::invoke()->getList($page, $limit, $searchQuery);
        $data = [];
        if ($list['list']) {
            foreach ($list['list'] as &$item) {
                $data[] = ['device_no' => $item['device_no']];
            }
        }
        $this->response->success($data);
    }

    public function save()
    {
        $data = input('post.', []);
        $id = input('post.id', 0);
        $searchQuery['temple_status'] = 2;
        $rs = LotusLampService::invoke()->save($data, $id);
        $this->response->success($rs);
    }

    public function monitorList()
    {
        $page = input('get.page', 1);
        $limit = input('get.limit', 50);
        $searchQuery['temple_status'] = 2;
        $list = MonitorService::invoke()->getList($page, $limit, $searchQuery);
        $data = [];
        if ($list['list']) {
            foreach ($list['list'] as &$item) {
                $data[] = ['device_no' => $item['device_no']];
            }
        }
        $this->response->success($data);
    }

    /**
     * 绑定硬件
     */
    public function bind()
    {
        $data = input('post.', []);
        $id = input('post.id', 0);
        $rs = LotusLampService::invoke()->bind($data, $id);
        $this->response->success($rs);
    }

    /**
     * 删除寺庙
     */
    public function del()
    {
        $id = input('post.id');
        LotusLampService::invoke()->del($id);
        $this->response->success();
    }

    /**
     * 获取小程序图片链接
     */
    public function get_lamp_qrcode()
    {
        $id = input('get.id', 0);
        $temple_id = input('get.temple_id', 1);
        $info = LotusLampService::invoke()->info($id);
        if ($info) {
            $temple_id = $info['temple_id'];
        }

        $url = "https://app.fjxmlz.com/htmls/#/pages/pray/pray?temple_id=$temple_id&lotus_lamp_ids=" . $id;
        $rs = QrCodeService::invoke()->getCode($url);
        $this->response->success($rs);
    }

    /**
     * 获取二维码图片链接
     */
    public function getImage()
    {
        $url = input('post.url', '');

        return LotusLampService::invoke()->downloadFile($url);
    }

    /**
     * 批量更新新增
     */
    public
    function updateAll()
    {
        $rs = LotusLampService::invoke()->updateAll();
        $this->response->success($rs);
    }

    /**
     * 维修申请列表
     */
    public
    function repairRequestList()
    {
        $this->response->success([]);
    }
}