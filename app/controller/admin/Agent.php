<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\AgentService;

class Agent extends ApiController
{
    /**
     * 代理商列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = AgentService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 业务员列表
     */
    public function salesmanList()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = AgentService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 用户列表
     */
    public function memberList()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = AgentService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }


}