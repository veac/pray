<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\LightSupplyService;
use app\service\CateService;

class LightSupply extends ApiController
{
    /**
     * 祈福心愿列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = LightSupplyService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function cateList()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = CateService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function save()
    {
        $data = input('post.', []);
        $id = input('post.id', null);
        LightSupplyService::invoke()->save($data, $id);
        $this->response->success([]);
    }

    public function saveCate()
    {
        $data = input('post.', []);
        $cate_id = input('post.cate_id', null);
        CateService::invoke()->save($data, $cate_id);
        $this->response->success([]);
    }

    public function cateDel()
    {
        $id = input('post.id', null);
        CateService::invoke()->del($id);
        $this->response->success([]);
    }

    public function del()
    {
        $id = input('post.id', null);
        LightSupplyService::invoke()->del($id);
        $this->response->success([]);
    }

}