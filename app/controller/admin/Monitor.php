<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\MonitorService;

class Monitor extends ApiController
{
    /**
     * 显示器列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = MonitorService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

}