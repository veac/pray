<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\CategoryService;

class Category extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $list = CategoryService::invoke()->getList($page, $limit);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = CategoryService::invoke()->info($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        CategoryService::invoke()->save($data);
        $this->response->success(true);
    }


    public function del()
    {
        $id = input('post.id');
        CategoryService::invoke()->del($id);
        $this->response->success();
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.id');
        CategoryService::invoke()->save($data, $id);
        $this->response->success(true);
    }


}