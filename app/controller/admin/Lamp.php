<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\LampService;

class Lamp extends ApiController
{
    /**
     * 路灯列表
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['user_id'] = Request::invoke()->getUser('user_id');
        $list = LampService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 批量添加路灯编号
     */
    public function addAll()
    {
        $data = input('post.');
        LampService::invoke()->add($data);
        $this->response->success(true);
    }

    /**
     * 删除路灯
     */
    public function del()
    {
        $id = input('post.id');
        LampService::invoke()->del($id);
        $this->response->success(true);
    }

    /**
     * 编辑保存路灯编号
     */
    public function save()
    {
        $data = input('post.');
        $id = input('post.id', null);
        LampService::invoke()->save($data, $id);
        $this->response->success(true);
    }
}