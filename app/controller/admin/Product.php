<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\CompanyService;
use app\service\GoodsService;
use app\service\ProductService;

class Product extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = ProductService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.product_id');
        $info = ProductService::invoke()->info($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        ProductService::invoke()->save($data);
        $this->response->success(true);
    }


    public function del()
    {
        $id = input('post.product_id');
        ProductService::invoke()->del($id);
        $this->response->success();
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.product_id');
        ProductService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function saveStatus()
    {
        $status = input('post.status');
        $id = input('post.product_id');
        ProductService::invoke()->saveStatus($status, $id);
        $this->response->success(true);
    }
}