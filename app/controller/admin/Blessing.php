<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\BlessingService;

class Blessing extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = BlessingService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = BlessingService::invoke()->info($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        BlessingService::invoke()->save($data);
        $this->response->success(true);
    }

    public function del()
    {
        $id = input('post.id');
        BlessingService::invoke()->del($id);
        $this->response->success();
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.id');
        BlessingService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function typeList()
    {
        $list = [
            ['id' => 1, 'name' => '逢考必过'],
            ['id' => 2, 'name' => '财源滚滚'],
            ['id' => 3, 'name' => '健康吉祥'],
            ['id' => 4, 'name' => '平安喜乐'],
            ['id' => 5, 'name' => '步步高升'],
            ['id' => 6, 'name' => '子孙满堂'],
            ['id' => 7, 'name' => '姻缘天成'],
        ];
        $this->response->success($list);
    }
}