<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\DeptService;

class Dept extends ApiController
{
    public function tree()
    {
        $tree = DeptService::invoke()->getDeptTree();
        $this->response->success($tree);
    }

    public function list()
    {
        $searchQuery = input('post.searchQuery', null);
        $data = DeptService::invoke()->list($searchQuery);
        $this->response->success($data);
    }

    public function del()
    {
        $dept_id = input('post.dept_id');
        DeptService::invoke()->del($dept_id);
        $this->response->success(true);
    }

    public function create()
    {
        $data = input('post.');
        DeptService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $dept_id = input('post.dept_id');
        DeptService::invoke()->save($data, $dept_id);
        $this->response->success(true);
    }

    public function info()
    {
        $dept_id = input('get.dept_id');
        $data = DeptService::invoke()->info($dept_id);
        $this->response->success($data);
    }
}