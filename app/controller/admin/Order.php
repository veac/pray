<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\OrderService;

/**
 *订单列表
 */
class Order extends ApiController
{
    public function orderList()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = OrderService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }


}