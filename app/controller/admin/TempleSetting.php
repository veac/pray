<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\TempleSettingService;

class TempleSetting extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = TempleSettingService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = TempleSettingService::invoke()->info($id);
        $this->response->success($info);
    }

    public function save()
    {
        $data = input('post.');
        $id = input('post.id', null);
        TempleSettingService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function del()
    {
        $id = input('post.id');
        TempleSettingService::invoke()->del($id);
        $this->response->success();
    }
}