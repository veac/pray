<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\PostService;

class Post extends ApiController
{
    public function select()
    {
        $res = PostService::invoke()->selectPosts();
        $this->response->success($res);
    }

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.searchQuery', null);

        $data = PostService::invoke()->list($page, $limit, $searchQuery);

        $this->response->success($data);
    }

    public function del()
    {
        $dept_id = input('post.post_id');
        PostService::invoke()->del($dept_id);
        $this->response->success(true);
    }

    public function create()
    {
        $data = input('post.');
        PostService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $post_id = input('post.post_id');
        PostService::invoke()->save($data, $post_id);
        $this->response->success(true);
    }

    public function info()
    {
        $dept_id = input('get.post_id');
        $data = PostService::invoke()->info($dept_id);
        $this->response->success($data);
    }
}