<?php

namespace app\controller\admin;


use app\core\ApiController;
use app\service\MenuService;

/**
 * 菜单管理
 */
class Menu extends ApiController
{
    public function list()
    {
        $appKey = input('post.app_key');
        $searchQuery = input('post.searchQuery/a', null);
        $list = MenuService::invoke()->getSysMenuList($appKey, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = MenuService::invoke()->getMenuInfo($id);
        $this->response->success($info);
    }

    public function create()
    {
        $data = input('post.');
        MenuService::invoke()->save($data);
        $this->response->success(true);
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.menu_id');
        MenuService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function tree()
    {
        $appKey = input('get.app_key');
        $tree = MenuService::invoke()->getMenuTree($appKey);
        $this->response->success($tree);
    }

    public function del()
    {
        $id = input('post.id');
        MenuService::invoke()->delById($id);
        $this->response->success(true);
    }
}