<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\IncomeService;

class Income extends ApiController
{
    /**
     * 收益明细
     */
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = IncomeService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    /**
     * 分销佣金订单
     */
    public function commissionOrder()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $searchQuery['types'] = [1, 2];
        $list = IncomeService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }
}