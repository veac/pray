<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\PlatformRulesService;

class PlatformRules extends ApiController
{
    /**
     * 祈福规则
     */
    public function info()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = PlatformRulesService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }
}