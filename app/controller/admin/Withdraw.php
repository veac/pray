<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\core\Request;
use app\service\BankService;
use app\service\TempleService;
use app\service\WithdrawService;

class Withdraw extends ApiController
{
    /**
     * 获取提现明细
     */
    public function list()
    {
        $page = input('get.page', 1);
        $limit = input('get.limit', 10);
        $searchQuery = input('get.', []);
        $list = WithdrawService::invoke()->getList($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function getBank()
    {
        $id = input('get.bank_id', 0);
        $info = BankService::invoke()->info($id);
        $this->response->success($info);
    }

    public function saveStatus()
    {
        $id = input('post.id', 0);
        $data = input('post.');
        $info = WithdrawService::invoke()->update($data, $id);
        $this->response->success($info);
    }

    public function info()
    {
        $id = input('get.id', 0);

        $info = WithdrawService::invoke()->info($id);
        $this->response->success($info);
    }
}