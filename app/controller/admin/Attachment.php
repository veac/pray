<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\AttachmentService;
use app\core\Request;

class Attachment extends ApiController
{
    /**
     * 附件上传
     * @return void
     */
    public function upload()
    {
        $parent_id = input('post.parent_id', 0);
        $file = $this->request->file('file');
        $res = AttachmentService::invoke()->upload($file, $parent_id);
        $this->response->success($res);
    }

    public function uploadfile()
    {
        $parent_id = input('post.parent_id', 0);
        $file = $this->request->file('file');
        $res = AttachmentService::invoke()->uploadfile($file, $parent_id);
        $this->response->success($res);
    }

    public function show()
    {
        $path = input('get.path');
        if (empty($path)) {
            $this->response
                ->content("not found.")
                ->code(404)
                ->send();
        }
        AttachmentService::invoke()->show($path);
    }
}