<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\MemberService;

class Member extends ApiController
{
    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 10);
        $searchQuery = input('post.', []);
        $list = MemberService::invoke()->list($page, $limit, $searchQuery);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = MemberService::invoke()->info($id);
        $this->response->success($info);
    }

}