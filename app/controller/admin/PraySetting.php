<?php

namespace app\controller\admin;

use app\core\ApiController;
use app\service\PraySettingService;

class PraySetting extends ApiController
{

    public function list()
    {
        $page = input('post.page', 1);
        $limit = input('post.limit', 30);
        $list = PraySettingService::invoke()->list($page, $limit);
        $this->response->success($list);
    }

    public function info()
    {
        $id = input('get.id');
        $info = PraySettingService::invoke()->info($id);
        $this->response->success($info);
    }

    public function save()
    {
        $data = input('post.');
        $id = input('post.id', null);
        PraySettingService::invoke()->save($data, $id);
        $this->response->success(true);
    }

    public function del()
    {
        $id = input('post.id');
        PraySettingService::invoke()->del($id);
        $this->response->success();
    }
}