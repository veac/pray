<?php

namespace app\service;

use app\core\Constant;
use app\core\exception\BusinessException;
use app\core\Request;
use app\core\Service;
use app\core\util\Encryp;
use app\model\Attachment;
use app\model\Policy;
use app\model\SysUser;
use app\model\SysUserPost;
use app\model\SysUserRole;
use think\facade\Db;

class PolicyService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];

        $policy = Db::table('policy')->where($w)->where('id', 'not null')->page($page, $limit);
        $total = $policy->count();
        $policy = $policy->select()->toArray();

        foreach ($policy as &$item) {
            $item['file_url'] = json_decode($item['file_url'],true);
            if (!empty($item['file_url'])) {
                $attachmentList = Attachment::where('attachment_id', 'in', $item['file_url'])->select();
                if ($attachmentList) {
                    $attachmentList = $attachmentList->toArray();
                    foreach ($attachmentList as $attachment) {
                        $item['file'][] = [
                            'id' => $attachment['attachment_id'],
                            'name' => $attachment['file_name'],
                            'url' => $attachment['path'],
                        ];
                    }
                }

            } else {
                $item['file'] = [];
            }
        }
        return [
            'total' => $total,
            'list' => $policy,
        ];
    }

    public function save($data, $id = null)
    {
        $saveData = [
            "title" => $data['title'],
            "file_url" => json_encode($data['file_url']),
            "content" => $data['content'],
            "outside_url" => $data['outside_url'],
        ];
        try {
            if (!empty($id)) {
                $policy = Policy::scope(['data'])->where('id', $id)->find();
                $policy->save($saveData);
            } else {
                $policy = Policy::create($saveData);
                if (empty($policy)) {
                    throw new BusinessException('创建失败');
                }
            }
        } catch (\Exception $e) {
            throw new BusinessException($e->getMessage());
        }

        return true;
    }

    public function info($id)
    {
        $policy = Policy::find($id);
        if (!$policy) {
            throw new BusinessException('部门不存在');
        }
        $domain = env('filesystem.host', Request::invoke()->domain());
        $policy = $policy->toArray();
        $policy['file_url'] = json_decode($policy['file_url'],true);
        if (!empty($policy['file_url'])) {
            $attachmentList = Attachment::where('attachment_id', 'in', $policy['file_url'])->select();
            if ($attachmentList) {
                $attachmentList = $attachmentList->toArray();
                foreach ($attachmentList as $attachment) {
                    $policy['file'][] = [
                        'id' => $attachment['attachment_id'],
                        'name' => $attachment['file_name'],
                        'url' => $domain . $attachment['path'],
                    ];
                }
            }

        } else {
            $policy['file'] = [];
        }
        return $policy;
    }
}