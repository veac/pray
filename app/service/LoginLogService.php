<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\SysLoginLog;

/**
 * @extends Service<LoginLogService>
 */
class LoginLogService extends Service
{

    public function list($page = 1, $limit = 10, $searchQuery = null)
    {
        $list = SysLoginLog::where(function ($q) use ($searchQuery) {
            if (!empty($searchQuery['user_name'])) {
                $q = $q->where('user_name', 'like', "%" . $searchQuery['user_name'] . "%");
            }
            if (!empty($searchQuery['login_time'])) {
                $time_arr = explode("#", $searchQuery['login_time']);
                if (count($time_arr) == 2) {
                    $q = $q->whereTime('login_time', 'between', $time_arr);
                }
            }
            if (!empty($searchQuery['login_status'])) {
                $q->where('login_status', $searchQuery['login_status']);
            }
        });

        $total = $list->count();
        $list = $list->order('login_log_id desc')
            ->page($page, $limit)
            ->select()->toArray();

        return ['total' => $total, 'list' => $list];
    }

    public function info($login_log_id)
    {
        $login_log = SysLoginLog::find($login_log_id);

        if (!$login_log){
            throw new BusinessException('日志不存在');
        }

        return $login_log;
    }
}