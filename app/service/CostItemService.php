<?php

namespace app\service;

use app\core\Service;
use app\model\CostItem;

/**
 * @extends Service<CostItemService>
 */
class CostItemService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $activity = CostItem::where([])->field('id,name')->page($page, $limit);
        return [
            'list' => $activity->select(),
        ];
    }

}