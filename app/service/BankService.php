<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\Bank;
use think\facade\Db;

/**
 * @extends Service<BankService>
 */
class BankService extends Service
{

    public function list($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['type'])) {
            $w[] = ['type', '=', $searchQuery['type']];
        }
        $data = Bank::where($w)->page($page, $limit);
        $list = $data->select()->toArray();
        $bank = $this->getBank();
        $bank = array_column($bank, null, 'name');
        foreach ($list as &$value) {
            if ($value['bank_open']) {
                $value['img'] = $bank[$value['bank_open']]['img'];
                $value['colour'] = $bank[$value['bank_open']]['colour'];
            } else {
                $value['img'] = '';
            }

        }
        return [
            'total' => $data->count(),
            'list' => $list,
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            if ($data['type'] == 3) {//1-支付宝 2-微信 3-银行卡
                $info = Bank::where('bank_card', $data['bank_card'])->find();
                if (!empty($info)) {
                    throw new \Exception('银行卡信息已存在');
                }
                $map = [
                    'name' => $data['name'] ?: '',
                    'bank_card' => $data['bank_card'] ?: '',
                    'mobile' => $data['mobile'] ?: '',
                    'bank_open' => $data['bank_open'] ?: '',
                    'bank_branch' => $data['bank_branch'] ?: '',
                    'type' => $data['type'],
                    'member_id' => $data['member_id'],
                ];
            } else {
                $info = Bank::where('type', $data['type'])->find();
                if (!empty($info)) {
                    throw new \Exception('二维码已存在');
                }
                $map = [
                    'name' => $data['name'] ?: '',
                    'image' => $data['image'] ?: '',
                    'type' => $data['type'],
                    'member_id' => $data['member_id'],
                ];
            }
            if (empty($id)) {
                Bank::create($map);
            } else {
                $info = Bank::where('id', $id)->find();
                if (empty($info)) {
                    throw new \Exception('数据不存在');
                }
                $info->save($map);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }
        return true;
    }

    public function info($id, $member_id = 0, $type = 0)
    {
        $w = [];
        if ($member_id) {
            $w['member_id'] = $member_id;
        }
        if ($type) {
            $w['type'] = $type;
        }
        if ($id) {
            $w['id'] = $id;
        }
        $info = Bank::where($w)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info;
    }

    public function getBank()
    {
        $data = [
            ['name' => '中国人民银行', 'img' => 'storage/bank/1.png', 'colour' => '#9e1f31FF'],
            ['name' => '建设银行', 'img' => 'storage/bank/2.png', 'colour' => '#005babFF'],
            ['name' => '光大银行', 'img' => 'storage/bank/3.png', 'colour' => '#f5d992FF'],
            ['name' => '邮政储蓄银行', 'img' => 'storage/bank/4.png', 'colour' => '#007d3eFF'],
            ['name' => '农业银行', 'img' => 'storage/bank/5.png', 'colour' => '#009882FF'],
            ['name' => '交通银行', 'img' => 'storage/bank/6.png', 'colour' => '#00367aFF'],
            ['name' => '兴业银行', 'img' => 'storage/bank/7.png', 'colour' => '#004186FF'],
            ['name' => '工商银行', 'img' => 'storage/bank/8.png', 'colour' => '#d62629FF'],
            ['name' => '民生银行', 'img' => 'storage/bank/9.png', 'colour' => '#5aa572FF'],
            ['name' => '中信银行', 'img' => 'storage/bank/10.png', 'colour' => '#d7000fFF'],
            ['name' => '浦发银行', 'img' => 'storage/bank/11.png', 'colour' => '#074195FF'],
            ['name' => '招商银行', 'img' => 'storage/bank/12.png', 'colour' => '#e50012FF'],
            ['name' => '广发银行', 'img' => 'storage/bank/13.png', 'colour' => '#730000FF'],
            ['name' => '平安银行', 'img' => 'storage/bank/14.png', 'colour' => '#ff4801FF'],
        ];
        return $data;
    }
}