<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\Goods;
use app\model\Product;
use think\facade\Db;

/**
 * @extends Service<GoodsService>
 */
class GoodsService extends Service
{
    public function list($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['goods_name'])) {
            $w[] = ['goods_name', 'like', "%" . $searchQuery['goods_name'] . "%"];
        }
        $w[] = ['status', '=', 10];
        $w[] = ['is_delete', '=', 0];
        $map = Goods::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['goods_name'])) {
            $w[] = ['goods_name', 'like', "%" . $searchQuery['goods_name'] . "%"];
        }
        if (!empty($searchQuery['goods_id']) && $searchQuery['goods_id'] != '') {
            $w[] = ['goods_id', '=', $searchQuery['goods_id']];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        if (!empty($searchQuery['goods_no']) && $searchQuery['goods_no'] != '') {
            $w[] = ['goods_no', '=', $searchQuery['goods_no']];
        }
        $w[] = ['is_delete', '=', 0];
        $map = Goods::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            if ($data['user_type'] == 4 && empty($data['member_ids'])) {
                throw new \Exception('指定用户id必选不存在');
            }
            if (empty($id)) {
                Goods::create([
                    'goods_name' => $data['goods_name'],
                    'goods_price' => $data['goods_price'],
                    'goods_no' => 'GD' . time(),
                    'content' => $data['content'],
                    'stock' => $data['stock'],
                    'sales_initial' => $data['sales_initial'],
                    'sort' => $data['sort'],
                    'status' => $data['status'],
                    'goods_img' => $data['goods_img'],
                    'primary_income' => $data['primary_income'],
                    'secondary_income' => $data['secondary_income'],
                    'daily_income' => $data['daily_income'],
                    'is_refund' => $data['is_refund'],
                    'user_type' => $data['user_type'],
                    'member_ids' => !empty($data['member_ids']) ? $data['member_ids'] : '',
                ]);
            } else {
                $info = Goods::where('goods_id', $id)->find();
                if (empty($info)) {
                    throw new \Exception('数据不存在');
                }
                $goods = [];
                if ($data['goods_name']) {
                    $goods['goods_name'] = $data['goods_name'];
                }
                if ($data['goods_price']) {
                    $goods['goods_price'] = $data['goods_price'];
                }
                if ($data['content']) {
                    $goods['content'] = $data['content'];
                }
                if ($data['stock']) {
                    $goods['stock'] = $data['stock'];
                }
                if ($data['sales_initial']) {
                    $goods['sales_initial'] = $data['sales_initial'];
                }
                if ($data['sort']) {
                    $goods['sort'] = $data['sort'];
                }
                if ($data['status']) {
                    $goods['status'] = $data['status'];
                }
                if ($data['goods_img']) {
                    $goods['goods_img'] = $data['goods_img'];
                }
                if ($data['primary_income']) {
                    $goods['primary_income'] = $data['primary_income'];
                }
                if ($data['secondary_income']) {
                    $goods['secondary_income'] = $data['secondary_income'];
                }
                if ($data['daily_income']) {
                    $goods['daily_income'] = $data['daily_income'];
                }
                if ($data['is_refund']) {
                    $goods['is_refund'] = $data['is_refund'];
                }
                if ($data['user_type']) {
                    $goods['user_type'] = $data['user_type'];
                }
                if ($data['user_type'] == 4) {
                    $goods['member_ids'] = $data['member_ids'];
                } else {
                    $goods['member_ids'] = '';
                }
                $info->save($goods);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }

        return true;
    }

    public function info($goods_id)
    {
        $info = Goods::where('goods_id', $goods_id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }

        return $info->toArray();
    }

    public function saveStatus($status, $goods_id)
    {
        $info = Goods::where('goods_id', $goods_id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        $info->save([
            'status' => $status,
        ]);
        return true;
    }

    public function del($id)
    {
        $info = Goods::where('goods_id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info->save(['is_delete' => 1]);
    }
}