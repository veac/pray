<?php

namespace app\service;

use app\core\Service;
use app\model\LampApply;

/**
 * @extends Service<LampApplyService>
 */
class LampApplyService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['product_name'])) {
            $w[] = ['product_name', 'like', "%" . $searchQuery['product_name'] . "%"];
        }
        if (!empty($searchQuery['product_status']) && $searchQuery['product_status'] != '') {
            $w[] = ['product_status', '=', $searchQuery['product_status']];
        }
        $map = LampApply::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

}