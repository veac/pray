<?php

namespace app\service;

use app\core\Service;
use app\model\Notice;

/**
 * @extends Service<NoticeService>
 */
class NoticeService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['title'])) {
            $w[] = ['title', 'like', "%" . $searchQuery['title'] . "%"];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        $map = Notice::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

}