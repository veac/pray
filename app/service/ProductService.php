<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\core\util\http;
use app\model\Goods;
use app\model\Product;
use app\model\Company;
use app\model\SysUser;
use think\facade\Db;

/**
 * @extends Service<ProductService>
 */
class ProductService extends Service
{


    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        $w[] = ['is_delete', '=', 0];
        if (!empty($searchQuery['product_name'])) {
            $w[] = ['product_name', 'like', "%" . $searchQuery['product_name'] . "%"];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        if (!empty($searchQuery['product_no']) && $searchQuery['product_no'] != '') {
            $w[] = ['product_no', '=', $searchQuery['product_no']];
        }

        $map = Product::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

    public function list($page, $limit, $searchQuery = null)
    {
        $w = [];
        $w[] = ['is_delete', '=', 0];
        $w[] = ['status', '=', 10];
        if (!empty($searchQuery['is_hot'])) {
            $w[] = ['is_hot', '=', $searchQuery['is_hot']];
        }
        $map = Product::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            if (empty($id)) {
                Product::create([
                    'product_name' => $data['product_name'],
                    'product_price' => $data['product_price'],
                    'product_no' => 'PD' . time(),
                    'content' => $data['content'],
                    'stock' => $data['stock'],
                    'sales_initial' => $data['sales_initial'],
                    'sort' => $data['sort'],
                    'status' => $data['status'],
                    'product_img' => $data['product_img'],
                    'is_hot' => $data['is_hot'],
                ]);
            } else {
                $info = Product::where('product_id', $id)->find();
                if (empty($info)) {
                    throw new \Exception('数据不存在');
                }
                $product = [];
                if ($data['product_name']) {
                    $product['product_name'] = $data['product_name'];
                }
                if ($data['product_price']) {
                    $product['product_price'] = $data['product_price'];
                }
                if ($data['content']) {
                    $product['content'] = $data['content'];
                }
                if ($data['stock']) {
                    $product['stock'] = $data['stock'];
                }
                if ($data['sales_initial']) {
                    $product['sales_initial'] = $data['sales_initial'];
                }
                if ($data['sort']) {
                    $product['sort'] = $data['sort'];
                }
                if ($data['status']) {
                    $product['status'] = $data['status'];
                }
                if ($data['product_img']) {
                    $product['product_img'] = $data['product_img'];
                }
                if ($data['is_hot']) {
                    $product['is_hot'] = $data['is_hot'];
                }
                $info->save($product);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }

        return true;
    }

    public function info($product_id)
    {
        $info = Product::where('product_id', $product_id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        $info = $info->toArray();
        $info['freight'] = 10;
        $info['price'] = $info['product_price'] + $info['freight'];
        return $info;
    }


    public function del($id)
    {
        $info = Product::where('product_id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info->save(['is_delete' => 1]);
    }
    public function saveStatus($status, $id)
    {
        $info = Product::where('product_id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        $info->save([
            'status' => $status,
        ]);
        return true;
    }

}