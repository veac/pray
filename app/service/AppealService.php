<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\core\util\http;
use app\model\Appeal;
use app\model\ZtcUser;
use think\facade\Db;

/**
 * @extends Service<AppealService>
 */
class AppealService extends Service
{

    public function getCompanyList($page, $limit, $searchQuery = null)
    {
        $company = Appeal::where([])->page($page, $limit);
        return [
            'total' => $company->count(),
            'list' => $company->select(),
        ];
    }

    public function info($id)
    {
        $info = Appeal::where('id', $id)->find();
        if (!$info) {
            throw new BusinessException('诉求不存在');
        }

        return $info->toArray();
    }

    public function save($data, $user_id, $company_id = null)
    {
        $saveData = [
            'parent_id' => $data['parent_id'],
            'ancestors' => '0',
            'dept_name' => $data['dept_name'],
            'order_num' => $data['order_num'] ?? 99,
            'leader' => $data['leader'] ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'] ?? null,
            'status' => $data['status'] ?? '0'
        ];
    }


}