<?php

namespace app\service;

use app\core\Service;
use app\model\Agent;

/**
 * @extends Service<AgentService>
 */
class AgentService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['product_name'])) {
            $w[] = ['product_name', 'like', "%" . $searchQuery['product_name'] . "%"];
        }
        if (!empty($searchQuery['account']) && $searchQuery['account'] != '') {
            $w[] = ['account', '=', $searchQuery['account']];
        }
        if (!empty($searchQuery['nickname']) && $searchQuery['nickname'] != '') {
            $w[] = ['nickname', '=', $searchQuery['nickname']];
        }
        if (!empty($searchQuery['subMerchantId']) && $searchQuery['subMerchantId'] != '') {
            $w[] = ['sub_merchant_id', '=', $searchQuery['subMerchantId']];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        if (!empty($searchQuery['timeValues'])) {
            if (!is_array($searchQuery['timeValues'])) {
                $timeValues = json_decode($searchQuery['timeValues']);
            } else {
                $timeValues = $searchQuery['timeValues'];
            }
            $w[] = ['create_time', 'between', $timeValues];
        }
        $map = Agent::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

}