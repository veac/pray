<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\Lamp;
use think\facade\Db;

/**
 * @extends Service<LampService>
 */
class LampService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['device_no'])) {
            $w[] = ['device_no', 'like', "%" . $searchQuery['device_no'] . "%"];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        if (!empty($searchQuery['switch']) && $searchQuery['switch'] != '') {
            $w[] = ['switch', '=', $searchQuery['switch']];
        }
        if (!empty($searchQuery['temple_status']) && $searchQuery['temple_status'] != '') {
            $w[] = ['temple_status', '=', $searchQuery['temple_status']];
        }

        if (!empty($searchQuery['create_time'])) {
            $time_arr = explode("#", $searchQuery['create_time']);
            if (count($time_arr) > 0) {
                $w[] = ['create_time', 'between', $time_arr];
            }
        }
        $w[] = ['is_delete', '=', 0];
        $map = Lamp::where($w)->page($page, $limit);
        $list = $map->select()->toArray();
        if ($list) {
            foreach ($list as &$item) {
                $item['status_name'] = ($item['status'] == 1) ? '正常' : '关闭';
                $item['switch_name'] = ($item['switch'] == 1) ? '正常' : '关闭';
                $item['temple_status_name'] = ($item['temple_status'] == 1) ? '已绑定' : '未绑定';
            }
        }

        return [
            'total' => $map->count(),
            'list' => $list,
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            $info = Lamp::where('id', $id)->find();
            if (empty($info)) {
                throw new \Exception('数据不存在');
            }
            $info->save([
                'device_no' => $data['device_no'],
            ]);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }
        return true;
    }

    public function add($data)
    {
        $device_list = explode('
', $data['device_list']);
        if (empty($device_list)) {
            throw new \Exception('数据不存在');
        }
        Db::startTrans();
        try {
            $map = [];
            foreach ($device_list as $item) {
                $map[] = [
                    'device_no' => $item,
                    'status' => 1,
                    'switch' => 1,
                    'temple_status' => 1,
                    'third' => 0,
                ];
            }

            (new Lamp())->saveAll($map);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }
        return true;
    }

    public function del($id)
    {
        $info = Lamp::where('id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info->save(['is_delete' => 1]);
    }

}