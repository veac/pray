<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\SysOperLog;

/**
 * @extends Service<OperLogService>
 */
class OperLogService extends Service
{

    public function list($page = 1, $limit = 10, $searchQuery = null)
    {
        $list = SysOperLog::where(function ($q) use ($searchQuery) {
            if (!empty($searchQuery['app_key'])) {
                $q = $q->where('app_key', $searchQuery['app_key']);
            }
            if (!empty($searchQuery['oper_time'])) {
                $time_arr = explode("#", $searchQuery['oper_time']);
                if (count($time_arr) == 2) {
                    $q = $q->whereTime('oper_time', 'between', $time_arr);
                }
            }
            if (!empty($searchQuery['status'])) {
                $q->where('status', $searchQuery['status']);
            }
        });

        $total = $list->count();
        $list = $list->order('oper_id desc')
            ->page($page, $limit)
            ->select()->toArray();

        return ['total' => $total, 'list' => $list];
    }

    public function info($oper_log_id)
    {
        $oper_log = SysOperLog::find($oper_log_id);

        if (!$oper_log){
            throw new BusinessException('日志不存在');
        }

        return $oper_log;
    }
}