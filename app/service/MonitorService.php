<?php

namespace app\service;

use app\core\Service;
use app\model\Monitor;

/**
 * @extends Service<MonitorService>
 */
class MonitorService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['deviceNo'])) {
            $w[] = ['device_no', 'like', "%" . $searchQuery['deviceNo'] . "%"];
        }
        if (!empty($searchQuery['status']) && $searchQuery['status'] != '') {
            $w[] = ['status', '=', $searchQuery['status']];
        }
        if (!empty($searchQuery['switch']) && $searchQuery['switch'] != '') {
            $w[] = ['switch', '=', $searchQuery['switch']];
        }
        if (!empty($searchQuery['templeStatus']) && $searchQuery['templeStatus'] != '') {
            $w[] = ['temple_status', '=', $searchQuery['templeStatus']];
        }
        $map = Monitor::where($w)->page($page, $limit);
        $list = $map->select()->toArray();
        if ($list) {
            foreach ($list as &$item) {
                $item['status_name'] = ($item['status'] == 1) ? '正常' : '关闭';
                $item['switch_name'] = ($item['switch'] == 1) ? '正常' : '关闭';
                $item['temple_status_name'] = ($item['temple_status'] == 1) ? '已绑定' : '未绑定';
            }
        }

        return [
            'total' => $map->count(),
            'list' => $list,
        ];
    }

}