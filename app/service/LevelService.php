<?php

namespace app\service;

use app\core\Service;
use app\model\Level;

/**
 * @extends Service<LevelService>
 */
class LevelService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['product_name'])) {
            $w[] = ['product_name', 'like', "%" . $searchQuery['product_name'] . "%"];
        }
        if (!empty($searchQuery['product_status']) && $searchQuery['product_status'] != '') {
            $w[] = ['product_status', '=', $searchQuery['product_status']];
        }
        $map = Level::where($w)->page($page, $limit);
        return [
            'total' => $map->count(),
            'list' => $map->select(),
        ];
    }

}