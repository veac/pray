<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\Cate;
use app\model\LightSupply;
use think\facade\Db;

/**
 * @extends Service<LightSupplyService>
 */
class LightSupplyService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $w = [];

        if (!empty($searchQuery['name'])) {
            $w[] = ['name', 'like', "%" . $searchQuery['name'] . "%"];
        }
        if (!empty($searchQuery['cate_id']) && $searchQuery['cate_id'] != '') {
            $w[] = ['cate_id', '=', $searchQuery['cate_id']];
        }
        if (!empty($searchQuery['type']) && $searchQuery['type'] != '') {
            $w[] = ['type', '=', $searchQuery['type']];
        }
        if (!empty($searchQuery['timeValues'])) {
            if (!is_array($searchQuery['timeValues'])) {
                $timeValues = json_decode($searchQuery['timeValues']);
            } else {
                $timeValues = $searchQuery['timeValues'];
            }
            $w[] = ['create_time', 'between', $timeValues];
        }
        $w[] = ['is_delete', '=', 0];
        $map = LightSupply::where($w)->page($page, $limit)->order('sort', 'desc');
        $list = $map->select();
        if (!empty($list)) {
            $list = $list->toArray();
            foreach ($list as &$item) {
                $item['cate_name'] = Cate::where('cate_id', '=', $item['cate_id'])->value('name');
            }
        }
        return [
            'total' => $map->count(),
            'list' => $list,
        ];
    }

    public function list($searchQuery = null)
    {
        $w = [];
        if (!empty($searchQuery['cate_id']) && $searchQuery['cate_id'] != '') {
            $w[] = ['cate_id', '=', $searchQuery['cate_id']];
        }
        $w[] = ['is_delete', '=', 0];
        $list = LightSupply::where($w)->select()->toArray();
        return [
            'list' => $list,
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            if (empty($id)) {
                LightSupply::create([
                    'cate_id' => $data['cate_id'],
                    'name' => $data['name'],
                    'type' => $data['type'],
                    'sort' => $data['sort']
                ]);
            } else {
                $info = LightSupply::where('id', $id)->find();
                if (empty($info)) {
                    throw new \Exception('数据不存在');
                }
                $info->save([
                    'cate_id' => $data['cate_id'],
                    'name' => $data['name'],
                    'type' => $data['type'],
                    'sort' => $data['sort']
                ]);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }
        return true;
    }

    public function del($id)
    {
        $info = LightSupply::where('id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info->save(['is_delete' => 1]);
    }
}