<?php

namespace app\service;

use app\core\exception\BusinessException;
use app\core\Service;
use app\model\Blessing;
use think\facade\Db;

/**
 * @extends Service<BlessingService>
 */
class BlessingService extends Service
{
    public function getList($page, $limit, $searchQuery = null)
    {
        $category = Blessing::where([])->page($page, $limit);

        return [
            'total' => $category->count(),
            'list' => $category->select(),
        ];
    }

    public function save($data, $id = null)
    {
        Db::startTrans();
        try {
            if (empty($id)) {
                Blessing::create([
                    'name' => $data['name'],
                    'type' => $data['type'],
                ]);
            } else {
                $info = Blessing::where('id', $id)->find();
                if (empty($info)) {
                    throw new \Exception('数据不存在');
                }
                $info->save([
                    'name' => $data['name'],
                    'type' => $data['type'],
                ]);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw new BusinessException($e->getMessage());
        }

        return true;
    }

    public function info($id)
    {
        $info = Blessing::where('id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }

        return $info->toArray();
    }


    public function del($id)
    {
        $info = Blessing::where('id', $id)->find();
        if (!$info) {
            throw new BusinessException('数据不存在');
        }
        return $info->delete();
    }
}