<?php

namespace app\service;

use app\core\Service;
use app\core\util\http;
use think\facade\Log;

/**
 * @extends Service<PushService>
 */
class PushService extends Service
{
    public function pushWx($openId,$tmpId,$data)
    {
        $accTok = UserService::invoke()->getAccessToken();
        if(!$accTok) return;
        $header = [
            "Content-type: application/json;charset='utf-8'",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
        ];
        $url = 'https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=' . $accTok;
        $sendData = [
            'touser'      => $openId,
            'template_id' => $tmpId,
            'data'        => $data,
        ];
        $rspStr = Http::invoke()->request("POST", $url, [
            'json'    => $sendData,
            'timeout' => 3,
            'header'  => $header,
        ]);
        Log::write("subscribe.send:$rspStr");
    }
}