<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host' => env('app.host', ''),
    // 应用的命名空间
    'app_namespace' => '',
    // 是否启用路由
    'with_route' => true,
    // 默认应用
    'default_app' => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map' => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind' => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list' => [],

    // 异常页面的模板文件
    'exception_tmpl' => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message' => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg' => true,
    //支付宝app支付配置
    'alipay_rsa_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuwldb5qQzb+RDkhB5MzGrtqUX75cT+qxEIh5e+7YLq3HHLl9rHsI3dUdgZXMtuzFwB87i/qlfGYbt/8TfcGncNnKR+0uUS3L1Zte4pQLUUCPBsLRNx/qi6WNSvJLZ2NWRvQFaUbtibq3vwFYPvFhk0xls3U7sfIyyXrEV7u2WzzBl3FjfPIBlfjI71Prl6UZ5xTLQjyMbRksZQ07oWfmUxyu474soAbSXHAQsKUjghQdpTsH3EsxFYd/7Z/6bGAM/XDKOJfXuJIyjoXD5Y/LR21UJnFr/fybC6c1GOJVQxEIAo6ozA1WZkQTCsRYBVImez9s9tZEXzustu0zB9mA5QIDAQAB',
    'rsa_private_key' => 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCJnSD8OySfp1pgig9puFzs1I/yGyJ5r5vAM74l0nYVaFf/PuIBa6408XBy+FSzJTy8MEPsso5Oh7pLvboqwWXGeYmi9+ssCIX95dpNUhoyVTuj7YLP9Kl8iZhE3OUD7Kdf4bRCxknMNU9yUAzLJlLb0h7Cfqevg+G/LdVOdBl9+1bec43uZnlGhaPG+azI3CU9d8zQfKupqdLTCUNwdpQDHo6PFyeChYBpLzlv7v07kc0PyxXCzlz0Q2z7LUSh281eU0VEJS1uCQvxi7IYI3836Ec6jcjcFRjhGrpSow80mMaaqdtou6ity7BDgfjjkuY+1I7yQWCKMFGS4VGPu1GZAgMBAAECggEAYVVkCUODyfDCudmswSgHgfNuxN1wawVMcoumiHXt04J0CzNdA4g4NXY4BPmkXvmNlcXFpVs90Emr+Pp+/NLBu7onIb+c9Q4qR8UVvO6/XK5weqf5iEESx+VCcCKVUUtHr0ejCj/Z3rguMzpvchjvLk/WNdcDn972AxTV9KGljjbzGMQPyxS7m1/lDFQQkKrJfC/eIHTloWK9L3QQNYZxUpiDaxmu81c8Tg2qAsjci/yon+KhEmQAJr3loIZAzkLJvXCzBIVG8he8ptumFl3niCpbA0z4DiJOn+WGVhUPC74qRbNr9NLz1Zj3oN1SDjBswb5U5+VsyhnM/zNeciITCQKBgQDk+z9i073k5lVmyN40/E+x9IcaLpblbCf5oXtPWdze+bXJ2ZDCF7BVEwh80pf8zp51yByD2WnQAt1XeaF4l33V/N7Dj3YG/xIrOe5Tdd74x8CNleq6tj75TSPpPe+6iVq79lQBp1KaN7wrfoZCcGjUQNxMSkcnmG3pPawkaDp+IwKBgQCZ2fl1dKdfMP556wDrvSemHFH79hSHeoJUzUycTSwcFbyi492T1erJgtSMH2fy9N52MXOaqkMfxAvDcnsT9Fg+qqlsPN9r7077rQU/KlNLH63EGuOyWqorXMIbTBuVFNoisxrTYYUbtnClOMzoejKXle1EPtWJabh4vaa8htsHEwKBgDCZuyxosMditAGq5K7uVqP4uKwv80sil6F+SsPe2Ohiq9j52qW3jW+hKI4FzMiVukBZhuPdQ34o/X3RjRohXU4L6+QqEO0POnX+R71jKT44d4AVr8qNX/ReER8yTC2VdbKrZ80qDHg7GC9LHYgAf999HMT8hvm6eqmks2LO1IttAoGAP2QTp/SfvdYXFC7jGz695nCGyzUycjvlFYHdjg0yHoRKTCdtNauPaOYSI4YRM4b3XmWzx0deDumdGzPBSJCGB5QT6LgQKo3Y0lQQ9FzBIxPzafjWVreKpAE9LTgnqT6FPoTjibMFwExkj38e1q76x4CL2XG2VD8ONhmWaNl7x6cCgYEAyHeKqMr4wu2xMDTDEKiyDQg+7GrvnOpIFs/CFfB39NJBSSNO0wnKCblCSp5TZ8PKhiSlGmw+gBHoNyW6AH5DVeQmj8hhREmhWyay8L6Jt46S81gAyvziHhX5TCixMitnkit/hUI8B4qOnf2i8MptSiAyj0Hyd35JHv9hXTn65oE=',
    'alipay_url' => 'https://openapi.alipay.com/gateway.do',
    'alipay_app_id' => '2021004142654260',
    //微信小程序支付配置
    'app_id' => 'wxabe696a5fa941c35',
    'app_secret' => '03da8edfed265a6000a01f08b8c8771f',
    'mch_id' => '1674337151',
    'app_key' => '3559ffcadedd01e6c008b4ec92a52df2',
    'api_key' => '3559ffcadedd01e6c008b4ec92a52df2',//回调通知//51657C6D659AE9306253A5E3CA2C6ED7
    //微信公众号
    'js_app_id' => 'wxe5e4a3efa4cb09db',
    'js_app_secret' => '982349d654a975bab5c0b767f0bf977d',
    //微信app
    'wx_app_id' => 'wx9d9abd71b687f331',
    'wx_mch_id' => '1674337151',
    'wx_app_secret' => '0096a0ccc190f33bf3d023bd2c974051',
];
