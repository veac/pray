<?php

return [
    'alg' => env('token.alg', 'HS256'),
    'key' => env('token.key', '123456'),
    'expire' => env('token.expire', 86400)
];