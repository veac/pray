<?php

use think\facade\Route;
use app\core\middleware\JwtMemberAuth;
use app\core\middleware\JwtTempleAuth;
use app\validate\Member;
use app\validate\Temple;
use app\validate\Address;

$groupName = 'api';

Route::group($groupName, function () {
    // 用户登录注册
    Route::group('member', function () {
        Route::post('reg', '\app\controller\api\Member::reg')
            ->name('api:member:reg')
            ->validate(Member::class, 'create');
        Route::post('login', '\app\controller\api\Member::login')
            ->name('api:member:login')
            ->validate(Member::class, 'login');
        Route::post('getCode', '\app\controller\api\Member::getCode')
            ->name('api:member:getCode')
            ->validate(Member::class, 'code');
        Route::post('reset', '\app\controller\api\Member::reset')
            ->name('api:member:reset')
            ->validate(Member::class, 'reset');
        Route::post('checkrealname', '\app\controller\api\Member::checkRealname')
            ->name('api:member:checkrealname')
            ->validate(Member::class, 'real_name');
        Route::get('getrealname', '\app\controller\api\Member::getRealname')
            ->name('api:member:getrealname');
        Route::post('loginOut', '\app\controller\api\Member::loginOut')
            ->name('api:member:loginOut');
        Route::post('del', '\app\controller\api\Member::del')
            ->name('api:member:del');
        Route::get('getInviteCode', '\app\controller\api\Member::getInviteCode')
            ->name('api:member:getInviteCode');
        Route::get('myteam', '\app\controller\api\Member::myTeam')
            ->name('api:member:myteam');
        Route::get('home', '\app\controller\api\Member::home')
            ->name('api:member:home');
        Route::get('TeamIncomeList', '\app\controller\api\Member::TeamIncomeList')
            ->name('api:member:TeamIncomeList');
        Route::get('IncomeList', '\app\controller\api\Member::IncomeList')
            ->name('api:member:IncomeList');
        Route::get('pointsList', '\app\controller\api\Member::pointsList')
            ->name('api:member:pointsList');
        Route::get('commissionRanking', '\app\controller\api\Member::commissionRanking')
            ->name('api:member:commissionRanking');
        Route::get('getLoginImage', '\app\controller\api\Member::getLoginImage')
            ->name('api:member:getLoginImage');
        Route::post('getRetrieveCode', '\app\controller\api\Member::getRetrieveCode')
            ->name('api:member:getRetrieveCode');
        Route::post('getTokenByCode', '\app\controller\api\Member::getTokenByCode')
            ->name('api:member:getTokenByCode');
        Route::post('getTokenLogin', '\app\controller\api\Member::getTokenLogin')
            ->name('api:member:getTokenLogin');
        Route::post('getuserphonenumber', '\app\controller\api\Member::getuserphonenumber')
            ->name('api:member:getuserphonenumber');
        Route::post('retrieve', '\app\controller\api\Member::retrieve')
            ->name('api:member:retrieve')
            ->validate(Member::class, 'reset');
        Route::get('meritList', '\app\controller\api\Member::meritList')
            ->name('api:member:meritList');
    });
    Route::group('goods', function () {
        Route::post('list', '\app\controller\api\Goods::list')
            ->name('api:goods:list');
        Route::post('detail', '\app\controller\api\Goods::detail')
            ->name('api:goods:detail');
        Route::get('incometask', '\app\controller\api\Goods::incomeTask')
            ->name('api:goods:incometask');
        Route::get('teamtask', '\app\controller\api\Goods::teamTask')
            ->name('api:goods:teamtask');
    });
    Route::group('index', function () {
        Route::get('index', '\app\controller\api\Index::index');
        Route::get('savePeople', '\app\controller\api\Index::savePeople');
        Route::post('getGdpAndPeople', '\app\controller\api\Index::getGdpAndPeople')
            ->name('api:index:getGdpAndPeople');
        Route::post('getHousePrice', '\app\controller\api\Index::getHousePrice')
            ->name('api:index:getHousePrice');
        Route::get('sug', '\app\controller\api\Index::sug');
        Route::get('searchPrice', '\app\controller\api\Index::searchPrice');
        Route::get('banner', '\app\controller\api\Index::banner')
            ->name('api:index:banner');
        Route::get('minisystem', '\app\controller\api\Index::minisystem')
            ->name('api:index:minisystem');
        Route::get('mini', '\app\controller\api\Index::mini')
            ->name('api:index:mini');
        Route::post('getgdp', '\app\controller\api\Index::getGdp')
            ->name('api:index:getgdp');
    });
    Route::group('Zqztc', function () {
        Route::get('saveUser', '\app\controller\api\Zqztc::saveUser');
        Route::get('saveCompany', '\app\controller\api\Zqztc::saveCompany');
        Route::get('saveInterview', '\app\controller\api\Zqztc::saveInterview');
        Route::get('saveEvaluate', '\app\controller\api\Zqztc::saveEvaluate');
    });
    // 附件管理
    Route::group('attachment', function () {
        // 附件上传
        Route::post('upload', '\app\controller\api\Attachment::upload')
            ->name('api:attachment:upload');
        Route::post('uploadfile', '\app\controller\api\Attachment::uploadfile')
            ->name('api:attachment:uploadfile');
        // 图片回显
        Route::get('show', '\app\controller\api\Attachment::show')
            ->name('api:attachment:show');
    });
    Route::group('order', function () {
        Route::post('create', '\app\controller\api\Order::create')//创建认购订单
        ->name('api:order:create');
        Route::post('mylist', '\app\controller\api\Order::myList')//认购列表-我的订单
        ->name('api:order:mylist');
        Route::post('myteamlist', '\app\controller\api\Order::myTeamList')//认购列表-我的团队
        ->name('api:order:myteamlist');
        Route::get('detail', '\app\controller\api\Order::detail')//认购列表-认购详情
        ->name('api:order:detail');
        Route::post('distribution', '\app\controller\api\Order::distribution')//我的-分销订单
        ->name('api:order:distribution');
        Route::get('incomelist', '\app\controller\api\Order::incomeList')//我的-分销订单
        ->name('api:order:incomelist');
    });
    // 寺庙
    Route::group('temple', function () {
        Route::post('list', '\app\controller\api\Temple::list')
            ->name('api:temple:list');
        Route::get('info', '\app\controller\api\Temple::info')
            ->name('api:temple:info');
        Route::get('getlamplist', '\app\controller\api\Temple::getLampList')
            ->name('api:temple:getlamplist');
        Route::get('deviceslist', '\app\controller\api\Temple::devicesList')
            ->name('api:temple:deviceslist');
        Route::get('projectslist', '\app\controller\api\Temple::projectsList')
            ->name('api:temple:projectslist');
        Route::post('login', '\app\controller\api\Temple::login')
            ->name('api:temple:login')
            ->validate(Temple::class, 'login');

    });
    // 提现
    Route::group('withdraw', function () {
        Route::post('list', '\app\controller\api\Withdraw::list')
            ->name('api:withdraw:list');
        Route::post('create', '\app\controller\api\Withdraw::create')
            ->name('api:withdraw:create');
    });
    Route::group('notice', function () {
        Route::post('list', '\app\controller\api\Notice::list')//公告列表
        ->name('admin:notice:list');
    });
    Route::group('notify', function () {
        Route::post('notice', '\app\controller\api\Notify::notice')
            ->name('api:notify:notice');
        Route::get('test', '\app\controller\api\Notify::test')
            ->name('api:notify:test');
        Route::post('alinotice', '\app\controller\api\Notify::aliNotice')
            ->name('api:notify:alinotice');
        Route::get('testadcs', '\app\controller\api\Notify::testadcs')
            ->name('api:notify:testadcs');
    });
    Route::group('TeamIncome', function () {
        Route::get('list', '\app\controller\api\TeamIncome::list')
            ->name('api:TeamIncome:list');
    });
    Route::group('bank', function () {
        Route::post('list', '\app\controller\api\Bank::list')
            ->name('api:bank:list');
        Route::post('save', '\app\controller\api\Bank::save')
            ->name('api:bank:save');
        Route::get('info', '\app\controller\api\Bank::info')
            ->name('api:bank:info');
        Route::get('getBank', '\app\controller\api\Bank::getBank')
            ->name('api:bank:getBank');
    });
    Route::group('address', function () {
        Route::post('list', '\app\controller\api\Address::list')
            ->name('api:address:list');
        Route::post('save', '\app\controller\api\Address::save')
            ->name('api:address:save')
            ->validate(Address::class, 'save');
        Route::post('del', '\app\controller\api\Address::del')
            ->name('api:address:del');
        Route::get('info', '\app\controller\api\Address::info')
            ->name('api:address:info');
    });
    Route::group('product', function () {
        Route::post('list', '\app\controller\api\Product::list')
            ->name('api:product:list');
        Route::get('info', '\app\controller\api\Product::info')
            ->name('api:product:info');
    });
    Route::group('productOrder', function () {
        Route::post('create', '\app\controller\api\ProductOrder::create')
            ->name('api:productOrder:create');
        Route::post('list', '\app\controller\api\ProductOrder::list')
            ->name('api:productOrder:list');
        Route::get('info', '\app\controller\api\ProductOrder::info')
            ->name('api:productOrder:info');
        Route::post('cancel', '\app\controller\api\ProductOrder::cancel')
            ->name('api:productOrder:cancel');
        Route::post('del', '\app\controller\api\ProductOrder::del')
            ->name('api:productOrder:del');
    });
    Route::group('blessingorder', function () {
        Route::post('create', '\app\controller\api\BlessingOrder::create')
            ->name('api:blessingorder:create');
        Route::post('list', '\app\controller\api\BlessingOrder::list')
            ->name('api:blessingorder:list');
        Route::get('info', '\app\controller\api\BlessingOrder::info')
            ->name('api:blessingorder:info');
        Route::post('cancel', '\app\controller\api\BlessingOrder::cancel')
            ->name('api:blessingorder:cancel');
        Route::post('del', '\app\controller\api\BlessingOrder::del')
            ->name('api:blessingorder:del');
        Route::get('ordertask', '\app\controller\api\BlessingOrder::orderTask')
            ->name('api:blessingorder:ordertask');
    });
    Route::group('pay', function () {
        Route::post('wxpay', '\app\controller\api\Pay::wxPay')
            ->name('api:pay:wxpay');
        Route::post('alipay', '\app\controller\api\Pay::aliPay')
            ->name('api:pay:aliPay');
        Route::post('wxapp', '\app\controller\api\Pay::wxApp')
            ->name('api:pay:wxapp');
    });
    Route::group('aftersale', function () {
        Route::post('apply', '\app\controller\api\AfterSale::apply')
            ->name('api:aftersale:apply');
        Route::get('getreason', '\app\controller\api\AfterSale::getReason')
            ->name('api:aftersale:getreason');
    });
    Route::group('lightsupply', function () {
        Route::get('list', '\app\controller\api\LightSupply::list')//祈福心愿列表
        ->name('api:lightsupply:list');
        Route::get('catelist', '\app\controller\api\LightSupply::cateList')//祈福心愿列表
        ->name('api:lightsupply:catelist');
    });
    Route::group('praysetting', function () {
        Route::post('list', '\app\controller\api\PraySetting::list')
            ->name('api:praysetting:list');
    });
    Route::group('synquery', function () {
        Route::get('index', '\app\controller\api\SynQuery::index')
            ->name('api:synquery:index');
    });
    Route::group('wechatpay', function () {
        Route::post('index', '\app\controller\api\WeChatPay::index')
            ->name('api:wechatpay:index');
    });

})->middleware(JwtMemberAuth::class, [
    'api:member:login',
    'api:member:reg',
    'api:member:getCode',
    'api:index:getGdpAndPeople',
    'api:index:getHousePrice',
    'api:goods:incometask',
    'api:goods:teamtask',
    'api:member:getLoginImage',
    'api:member:getRetrieveCode',
    'api:member:retrieve',
    'api:index:banner',
    'api:member:getTokenByCode',
    'api:member:getTokenLogin',
    'api:member:getuserphonenumber',
    'api:product:list',
    'api:product:info',
    'api:notify:notice',
    'api:notify:test',
    'admin:notice:list',
    'api:praysetting:list',
    'api:blessingorder:ordertask',
    'api:temple:info',
    'api:temple:list',
    'api:temple:deviceslist',
    'api:temple:projectslist',
    'api:notify:alinotice',
    'api:synquery:index',
    'api:index:getgdp',
    'api:index:minisystem',
    'api:index:mini',
    'api:temple:login',
]);
Route::group($groupName, function () {
    // 寺庙
    Route::group('temple', function () {
        Route::post('list', '\app\controller\api\Temple::list')
            ->name('api:temple:list');
        Route::get('info', '\app\controller\api\Temple::info')
            ->name('api:temple:info');
        Route::get('home', '\app\controller\api\Temple::home')
            ->name('api:temple:home');
        Route::get('getlamplist', '\app\controller\api\Temple::getLampList')
            ->name('api:temple:getlamplist');
        Route::get('deviceslist', '\app\controller\api\Temple::devicesList')
            ->name('api:temple:deviceslist');
        Route::get('projectslist', '\app\controller\api\Temple::projectsList')
            ->name('api:temple:projectslist');
        Route::post('login', '\app\controller\api\Temple::login')
            ->name('api:temple:login')
            ->validate(Temple::class, 'login');
    });
    // 提现
    Route::group('withdraw', function () {
        Route::post('list', '\app\controller\api\Withdraw::list')
            ->name('api:withdraw:list');
        Route::post('create', '\app\controller\api\Withdraw::create')
            ->name('api:withdraw:create');
    });
    Route::group('templebank', function () {
        Route::post('list', '\app\controller\api\TempleBank::list')
            ->name('api:templebank:list');
        Route::post('save', '\app\controller\api\TempleBank::save')
            ->name('api:templebank:save');
        Route::get('info', '\app\controller\api\TempleBank::info')
            ->name('api:templebank:info');
        Route::get('getBank', '\app\controller\api\TempleBank::getBank')
            ->name('api:templebank:getBank');
    });
})->middleware(JwtTempleAuth::class, [
    'api:temple:info',
    'api:temple:list',
    'api:temple:login',
]);