<?php

use app\core\middleware\JwtAuth;
use app\core\middleware\Permission;
use app\validate\Activity;
use app\validate\Expense;
use app\validate\Dept;
use app\validate\Menu;
use app\validate\Role;
use app\validate\Post;
use app\validate\User;
use app\validate\ZtcUser;
use think\facade\Route;

$groupName = 'admin';

Route::group($groupName, function () {
    // 登录
    Route::post('login', '\app\controller\admin\User::login')
        ->name('admin:login')
        ->validate(User::class, 'login');

    // 用户信息
    Route::get('get_user_info', '\app\controller\admin\User::getUserInfo')
        ->name("admin:get_user_info");
    // 访问日志
    Route::group('login_log', function () {
        Route::post('list', '\app\controller\admin\LoginLog::list')
            ->name('admin:login_log:list');
        Route::get('info', '\app\controller\admin\LoginLog::info')
            ->name('admin:login_log:info');
    });
    // 用户
    Route::group('user', function () {
        Route::get('info', '\app\controller\admin\User::info')
            ->name("admin:user:info");
        Route::get('menu_list', '\app\controller\admin\User::menuList')
            ->name('admin:user:menu_list');
        Route::post('list', '\app\controller\admin\User::list')
            ->name('admin:user:list');
        Route::post('create', '\app\controller\admin\User::create')
            ->name('admin:user:create')
            ->validate(User::class, 'create');
        Route::post('update', '\app\controller\admin\User::update')
            ->name('admin:user:update')
            ->validate(User::class, 'update');
        Route::post('del', '\app\controller\admin\User::del')
            ->name('admin:user:del');
        Route::post('select', '\app\controller\admin\User::select')
            ->name('admin:user:select');
        Route::post('bindWx', '\app\controller\admin\User::bindWx')
            ->name('admin:user:bindWx');
        Route::get('refreshAccToken', '\app\controller\admin\User::refreshAccToken')
            ->name('admin:user:refreshAccToken');
    });
    // 应用管理
    Route::group('app', function () {
        Route::post('list', '\app\controller\admin\App::list')
            ->name('admin:app:list');
        Route::get('info', '\app\controller\admin\App::info')
            ->name('admin:app:info');
        Route::post('create', '\app\controller\admin\App::create')
            ->name('admin:app:create')
            ->validate(\app\validate\App::class, 'create');
        Route::post('update', '\app\controller\admin\App::update')
            ->name('admin:app:update')
            ->validate(\app\validate\App::class, 'update');
        Route::post('del', '\app\controller\admin\App::del')
            ->name('admin:app:del');
        Route::get('select', '\app\controller\admin\App::select')
            ->name('admin:app:select');
    });
    // 角色
    Route::group('role', function () {
        Route::post('list', '\app\controller\admin\Role::list')
            ->name('admin:role:list');
        Route::get('info', '\app\controller\admin\Role::info')
            ->name('admin:role:info');
        Route::post('create', '\app\controller\admin\Role::create')
            ->name('admin:role:create')
            ->validate(Role::class, 'create');
        Route::post('update', '\app\controller\admin\Role::update')
            ->name('admin:role:update')
            ->validate(Role::class, 'update');
        Route::post('del', '\app\controller\admin\Role::del')
            ->name('admin:role:del');
        Route::get('select', '\app\controller\admin\Role::select')
            ->name('admin:role:select');
    });
    // 菜单
    Route::group('menu', function () {
        Route::post('list', '\app\controller\admin\Menu::list')
            ->name('admin:menu:list');
        Route::get('info', '\app\controller\admin\Menu::info')
            ->name('admin:menu:info');
        Route::post('create', '\app\controller\admin\Menu::create')
            ->name('admin:menu:create')
            ->validate(Menu::class, 'create');
        Route::post('update', '\app\controller\admin\Menu::update')
            ->name('admin:menu:update')
            ->validate(Menu::class, 'update');
        Route::post('del', '\app\controller\admin\Menu::del')
            ->name('admin:menu:del');
        Route::get('tree', '\app\controller\admin\Menu::tree')
            ->name('admin:menu:tree');
    });
    // 权限
    Route::group('permission', function () {
        Route::post('list', '\app\controller\admin\Permission::list')
            ->name('admin:permission:list');
        Route::get('info', '\app\controller\admin\Permission::info')
            ->name('admin:permission:info');
        Route::post('create', '\app\controller\admin\Permission::create')
            ->name('admin:permission:create')
            ->validate(\app\validate\Permission::class, 'create');
        Route::post('update', '\app\controller\admin\Permission::update')
            ->name('admin:permission:update')
            ->validate(\app\validate\Permission::class, 'create');
        Route::post('del', '\app\controller\admin\Permission::del')
            ->name('admin:permission:del');
        Route::get('tree', '\app\controller\admin\Permission::tree')
            ->name('admin:permission:tree');
    });
    // 部门
    Route::group('dept', function () {
        Route::post('list', '\app\controller\admin\Dept::list')
            ->name('admin:dept:list');
        Route::post('del', '\app\controller\admin\Dept::del')
            ->name('admin:dept:del');
        Route::post('create', '\app\controller\admin\Dept::create')
            ->name('admin:dept:create')
            ->validate(Dept::class, 'create');
        Route::post('update', '\app\controller\admin\Dept::update')
            ->name('admin:dept:update')
            ->validate(Dept::class, 'update');
        Route::get('info', '\app\controller\admin\Dept::info')
            ->name('admin:dept:info');
        Route::get('tree', '\app\controller\admin\Dept::tree')
            ->name('admin:dept:tree');
    });
    // 岗位
    Route::group('post', function () {
        Route::post('list', '\app\controller\admin\Post::list')
            ->name('admin:post:list');
        Route::post('del', '\app\controller\admin\Post::del')
            ->name('admin:post:del');
        Route::post('create', '\app\controller\admin\Post::save')
            ->name('admin:post:create')
            ->validate(Post::class, 'create');
        Route::post('update', '\app\controller\admin\Post::update')
            ->name('admin:post:update')
            ->validate(Post::class, 'update');
        Route::get('info', '\app\controller\admin\Post::info')
            ->name('admin:post:info');
        Route::get('select', '\app\controller\admin\Post::select')
            ->name('admin:post:select');
    });
    // 附件管理
    Route::group('attachment', function () {
        // 附件上传
        Route::post('upload', '\app\controller\admin\Attachment::upload')
            ->name('admin:attachment:upload');
        Route::post('uploadfile', '\app\controller\admin\Attachment::uploadfile')
            ->name('admin:attachment:uploadfile');
        // 图片回显
        Route::get('show', '\app\controller\admin\Attachment::show')
            ->name('admin:attachment:show');
    });

    // 类别
    Route::group('category', function () {
        Route::post('list', '\app\controller\admin\Category::list')
            ->name('admin:Category:list');
        Route::post('create', '\app\controller\admin\Category::create')
            ->name('admin:Category:create');
        Route::post('update', '\app\controller\admin\Category::update')
            ->name('admin:Category:update');
        Route::get('info', '\app\controller\admin\Category::info')
            ->name('admin:Category:info');
        Route::post('del', '\app\controller\admin\Category::del')
            ->name('admin:Category:del');
    });
    //认购商品
    Route::group('goods', function () {
        Route::post('list', '\app\controller\admin\Goods::list')
            ->name('admin:goods:list');
        Route::post('create', '\app\controller\admin\Goods::create')
            ->name('admin:goods:create');
        Route::post('update', '\app\controller\admin\Goods::update')
            ->name('admin:goods:update');
        Route::get('info', '\app\controller\admin\Goods::info')
            ->name('admin:goods:info');
        Route::post('del', '\app\controller\admin\Goods::del')
            ->name('admin:goods:del');
        Route::post('saveStatus', '\app\controller\admin\Goods::saveStatus')
            ->name('admin:goods:saveStatus');

    });
    // 商品
    Route::group('product', function () {
        Route::post('list', '\app\controller\admin\Product::list')
            ->name('admin:product:list');
        Route::post('create', '\app\controller\admin\Product::create')
            ->name('admin:product:create');
        Route::post('update', '\app\controller\admin\Product::update')
            ->name('admin:product:update');
        Route::get('info', '\app\controller\admin\Product::info')
            ->name('admin:product:info');
        Route::post('del', '\app\controller\admin\Product::del')
            ->name('admin:product:del');
        Route::post('saveStatus', '\app\controller\admin\Product::saveStatus')
            ->name('admin:product:saveStatus');
    });
    // 寺庙
    Route::group('temple', function () {
        Route::post('list', '\app\controller\admin\Temple::list')
            ->name('admin:temple:list');
        Route::post('create', '\app\controller\admin\Temple::create')
            ->name('admin:temple:create');
        Route::post('update', '\app\controller\admin\Temple::update')
            ->name('admin:temple:update');
        Route::get('info', '\app\controller\admin\Temple::info')
            ->name('admin:temple:info');
        Route::post('del', '\app\controller\admin\Temple::del')
            ->name('admin:temple:del');
        Route::post('save', '\app\controller\admin\Temple::save')
            ->name('admin:temple:save');
        Route::post('saveimage', '\app\controller\admin\Temple::saveImage')
            ->name('admin:temple:saveimage');
        Route::post('updateall', '\app\controller\admin\Temple::updateAll')
            ->name('admin:temple:updateall');
        Route::get('getcode', '\app\controller\admin\Temple::getCode')
            ->name('admin:temple:getcode');

    });
    // 祈福语
    Route::group('blessing', function () {
        Route::post('list', '\app\controller\admin\Blessing::list')
            ->name('admin:Blessing:list');
        Route::post('create', '\app\controller\admin\Blessing::create')
            ->name('admin:Blessing:create');
        Route::post('update', '\app\controller\admin\Blessing::update')
            ->name('admin:Blessing:update');
        Route::get('info', '\app\controller\admin\Blessing::info')
            ->name('admin:Blessing:info');
        Route::post('del', '\app\controller\admin\Blessing::del')
            ->name('admin:Blessing:del');
        Route::post('typeList', '\app\controller\admin\Blessing::typeList')
            ->name('admin:Blessing:typeList');
    });
    Route::group('blessingorder', function () {
        Route::post('list', '\app\controller\admin\BlessingOrder::list')
            ->name('admin:blessingorder:list');
        Route::get('info', '\app\controller\admin\BlessingOrder::info')
            ->name('admin:blessingorder:info');
        Route::post('export', '\app\controller\admin\BlessingOrder::export')
            ->name('admin:blessingorder:export');
    });
    Route::group('productorder', function () {
        Route::post('list', '\app\controller\admin\ProductOrder::list')
            ->name('admin:productorder:list');
        Route::get('info', '\app\controller\admin\ProductOrder::info')
            ->name('admin:productorder:info');
        Route::post('export', '\app\controller\admin\ProductOrder::export')
            ->name('admin:productorder:export');
        Route::post('checkreturn', '\app\controller\admin\ProductOrder::checkReturn')
            ->name('admin:productorder:checkreturn');
    });

    Route::group('push', function () {
        // 推送
        Route::get('pushInterViewTip', '\app\controller\admin\Push::pushInterViewTip')
            ->name("admin:Push:pushInterViewTip");
        Route::get('getWxTmpIds', '\app\controller\admin\Push::getWxTmpIds')
            ->name("admin:Push:getWxTmpIds");
    });
    Route::group('agent', function () {//代理商列表
        Route::post('list', '\app\controller\admin\Agent::list')
            ->name('admin:agent:list');
        Route::post('salesmanList', '\app\controller\admin\Agent::salesmanList')
            ->name('admin:agent:salesmanList');
    });
    Route::group('income', function () {
        Route::post('list', '\app\controller\admin\Income::list')//收益明细
        ->name('admin:income:list');
        Route::post('commissionOrder', '\app\controller\admin\Income::commissionOrder')//分佣订单
        ->name('admin:income:commissionOrder');
    });
    Route::group('lotuslamp', function () {
        Route::post('list', '\app\controller\admin\LotusLamp::list')//莲花灯列表
            ->name('admin:lotuslamp:list');
        Route::post('bind', '\app\controller\admin\LotusLamp::bind')//莲花灯列表
             ->name('admin:lotuslamp:bind');
        Route::get('get_lamp_qrcode', '\app\controller\admin\LotusLamp::get_lamp_qrcode')//莲花灯列表
             ->name('admin:lotuslamp:get_lamp_qrcode');
        Route::get('getlist', '\app\controller\admin\LotusLamp::getList')
            ->name('admin:lotuslamp:getlist');
        Route::get('monitorlist', '\app\controller\admin\LotusLamp::monitorList')
            ->name('admin:lotuslamp:monitorlist');
        Route::post('save', '\app\controller\admin\LotusLamp::save')
            ->name('admin:lotuslamp:save');
        Route::post('updateall', '\app\controller\admin\LotusLamp::updateAll')
            ->name('admin:lotuslamp:updateall');
        Route::post('del', '\app\controller\admin\LotusLamp::del')
            ->name('admin:lotuslamp:del');
        Route::get('info', '\app\controller\admin\LotusLamp::info')
            ->name('admin:lotuslamp:info');
        Route::post('repairRequestList', '\app\controller\admin\LotusLamp::repairRequestList')
            ->name('admin:lotuslamp:repairRequestList');
        Route::post('getimage', '\app\controller\admin\LotusLamp::getImage')
            ->name('admin:lotuslamp:getimage');

    });
    // 寺庙
    Route::group('temple', function () {
        Route::post('list', '\app\controller\admin\Temple::list')
            ->name('admin:Temple:list');
        Route::post('create', '\app\controller\admin\Temple::create')
            ->name('admin:Temple:create');
        Route::post('update', '\app\controller\admin\Temple::update')
            ->name('admin:Temple:update');
        Route::get('info', '\app\controller\admin\Temple::info')
            ->name('admin:Temple:info');
        Route::post('del', '\app\controller\admin\Temple::del')
            ->name('admin:Temple:del');
        Route::get('getqrcode', '\app\controller\admin\Temple::getQrcode')
            ->name('admin:Temple:getqrcode');
        Route::post('delimage', '\app\controller\admin\Temple::delImage')
            ->name('admin:Temple:delimage');


    });
    Route::group('order', function () {
        Route::post('orderlist', '\app\controller\admin\Order::orderList')//订单列表
        ->name('admin:order:orderlist');
    });
    Route::group('lamp', function () {
        Route::post('list', '\app\controller\admin\Lamp::list')//路灯列表
        ->name('admin:lamp:list');
        Route::post('save', '\app\controller\admin\Lamp::save')//路灯编号保存
        ->name('admin:lamp:save');
        Route::post('addall', '\app\controller\admin\Lamp::addAll')//批量路灯保存
        ->name('admin:lamp:addall');
        Route::post('del', '\app\controller\admin\Lamp::del')
            ->name('admin:lamp:del');

    });

    Route::group('lampapply', function () {
        Route::post('list', '\app\controller\admin\LampApply::list')//公德灯申请列表
        ->name('admin:lampapply:list');
    });
    Route::group('level', function () {
        Route::post('list', '\app\controller\admin\Level::list')//等级权益
        ->name('admin:level:list');
    });
    Route::group('lightsupply', function () {
        Route::post('list', '\app\controller\admin\LightSupply::list')//祈福心愿列表
        ->name('admin:lightsupply:list');
        Route::post('catelist', '\app\controller\admin\LightSupply::cateList')//祈福心愿列表
        ->name('admin:lightsupply:catelist');
        Route::post('save', '\app\controller\admin\LightSupply::save')
            ->name('admin:lightsupply:save');
        Route::post('saveCate', '\app\controller\admin\LightSupply::saveCate')
            ->name('admin:lightsupply:saveCate');
        Route::post('del', '\app\controller\admin\LightSupply::del')
            ->name('admin:lightsupply:del');
        Route::post('catedel', '\app\controller\admin\LightSupply::cateDel')
            ->name('admin:lightsupply:catedel');
    });
    Route::group('monitor', function () {
        Route::post('list', '\app\controller\admin\Monitor::list')//显示器列表
        ->name('admin:monitor:list');
    });
    Route::group('notice', function () {
        Route::post('list', '\app\controller\admin\Notice::list')//公告列表
        ->name('admin:notice:list');
    });
    Route::group('platformrules', function () {
        Route::post('info', '\app\controller\admin\PlatformRules::info')//祈福规则
        ->name('admin:platformrules:info');
    });
    Route::group('withdraw', function () {
        Route::get('list', '\app\controller\admin\Withdraw::list')
            ->name('api:withdraw:list');
        Route::get('getBank', '\app\controller\admin\Withdraw::getBank')
            ->name('api:withdraw:getBank');
        Route::get('info', '\app\controller\admin\Withdraw::info')
            ->name('api:withdraw:info');
        Route::post('saveStatus', '\app\controller\admin\Withdraw::saveStatus')
            ->name('api:withdraw:saveStatus');
    });
    //用户管理
    Route::group('member', function () {
        Route::post('list', '\app\controller\admin\Member::list')//公告列表
        ->name('admin:member:list');
        Route::get('info', '\app\controller\admin\Member::info')
            ->name('admin:member:info');
    });
    Route::group('settings', function () {
        Route::get('info', '\app\controller\admin\Settings::info')//设置-联系我们
        ->name('admin:settings:info');
        Route::post('save', '\app\controller\admin\Settings::save')//设置-联系我们
        ->name('admin:settings:save');
        Route::post('setCharge', '\app\controller\admin\Settings::setCharge')//全局配置手续费
        ->name('admin:settings:setCharge');
        Route::get('getCharge', '\app\controller\admin\Settings::getCharge')//全局配置手续费
        ->name('admin:settings:getCharge');
    });
    Route::group('praysetting', function () {
        Route::post('list', '\app\controller\admin\PraySetting::list')
            ->name('admin:praysetting:list');
        Route::get('info', '\app\controller\admin\PraySetting::info')
            ->name('admin:praysetting:info');
        Route::post('save', '\app\controller\admin\PraySetting::save')
            ->name('admin:praysetting:save');
        Route::post('del', '\app\controller\admin\PraySetting::del')
            ->name('admin:praysetting:del');
    });
    Route::group('templesetting', function () {
        Route::post('list', '\app\controller\admin\TempleSetting::list')
            ->name('admin:templesetting:list');
        Route::get('info', '\app\controller\admin\TempleSetting::info')
            ->name('admin:templesetting:info');
        Route::post('save', '\app\controller\admin\TempleSetting::save')
            ->name('admin:templesetting:save');
        Route::post('del', '\app\controller\admin\TempleSetting::del')
            ->name('admin:templesetting:del');
    });
    Route::group('banner', function () {
        Route::post('list', '\app\controller\admin\Banner::list')
            ->name('admin:banner:list');
        Route::post('save', '\app\controller\admin\Banner::save')
            ->name('admin:banner:save');
        Route::post('del', '\app\controller\admin\Banner::del')
            ->name('admin:banner:del');
    });
    Route::group('index', function () {
        Route::get('index', '\app\controller\admin\Index::index')
            ->name('admin:index:index');
        Route::post('getaddress', '\app\controller\admin\Index::getAddress')
            ->name('admin:index:getaddress');

    });

})->middleware(JwtAuth::class, [
    'admin:login',
    'admin:attachment:show',
    'admin:statistics:index',
    'admin:statistics:getInterview',
    'admin:user:refreshAccToken',
    'admin:Push:pushInterViewTip',
    'admin:attachment:uploadfile',
    'admin:attachment:upload',
    'admin:policy:list',
    'admin:Push:getWxTmpIds'
])->middleware(Permission::class, [
    'admin:login',
    'admin:company:export',
    'admin:interview:export',
    'admin:company:exportHelp',
    'admin:get_user_info',
    'admin:attachment:upload',
    'admin:attachment:show',
    'admin:attachment:uploadfile',
    'admin:user:menu_list',
    'admin:user:select',
    'admin:role:select',
    'admin:menu:tree',
    'admin:dept:tree',
    'admin:post:select',
    'admin:app:select',
    'admin:user:refreshAccToken',
    'admin:Push:pushInterViewTip',
    'admin:index:getaddress'
]);