-- 访问日志表
drop table if exists sys_login_log;
create table sys_login_log
(
    login_log_id   bigint(20) not null auto_increment comment '访问日志ID',
    user_name      varchar(50)  default '' comment '用户账号',
    ipaddr         varchar(128) default '' comment '登录IP地址',
    login_location varchar(255) default '' comment '登录地点',
    browser        varchar(50)  default '' comment '浏览器类型',
    os             varchar(50)  default '' comment '操作系统',
    login_status   char(1)      default '0' comment '登录状态（0成功 1失败）',
    msg            varchar(255) default '' comment '提示消息',
    login_time     datetime comment '访问时间',
    primary key (login_log_id),
    key idx_sys_loginlogr_s (login_status),
    key idx_sys_loginlogr_lt (login_time)
) engine = innodb
  auto_increment = 1 comment = '访问日志';

-- 初始化访问日志
insert into sys_login_log (`login_log_id`, `user_name`, `ipaddr`, `login_location`, `browser`, `os`, `login_status`,
                           `login_time`)
values (1, 'admin', '127.0.0.1', 'http://veac.wq.com:1921/admin/login', 'Chrome', 'Max OS', '0', sysdate());

-- 操作日志表
drop table if exists sys_oper_log;
create table sys_oper_log
(
    oper_id        bigint(20) not null auto_increment comment '日志主键',
    app_key        varchar(50)   default '' comment '应用标识',
    method         varchar(100)  default '' comment '方法名称',
    request_method varchar(10)   default '' comment '请求方式',
    oper_name      varchar(50)   default '' comment '操作人员',
    dept_name      varchar(50)   default '' comment '部门名称',
    oper_url       varchar(255)  default '' comment '请求URL',
    oper_ip        varchar(128)  default '' comment '主机地址',
    oper_location  varchar(255)  default '' comment '操作地点',
    oper_param     varchar(2000) default '' comment '请求参数',
    json_result    varchar(2000) default '' comment '返回参数',
    status         int(1)        default 0 comment '操作状态（0正常 1异常）',
    error_msg      varchar(2000) default '' comment '错误消息',
    oper_time      datetime comment '操作时间',
    cost_time      bigint(20)    default 0 comment '消耗时间',
    create_by      varchar(64)   default '' comment '创建者',
    create_time    datetime comment '创建时间',
    update_by      varchar(64)   default '' comment '更新者',
    update_time    datetime comment '更新时间',
    primary key (oper_id),
    key idx_sys_oper_log_s (status),
    key idx_sys_oper_log_ot (oper_time)
) engine = innodb
  auto_increment = 100 comment = '操作日志记录';

-- 部门表
drop table if exists sys_dept;
create table sys_dept
(
    dept_id     bigint(20) not null auto_increment comment '部门id',
    parent_id   bigint(20)  default 0 comment '父部门id',
    ancestors   varchar(50) default '' comment '祖级列表',
    dept_name   varchar(30) default '' comment '部门名称',
    order_num   int(4)      default 0 comment '显示顺序',
    leader      varchar(20) default null comment '负责人',
    phone       varchar(11) default null comment '联系电话',
    email       varchar(50) default null comment '邮箱',
    status      char(1)     default '0' comment '部门状态（0正常 1停用）',
    create_by   varchar(64) default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64) default '' comment '更新者',
    update_time datetime comment '更新时间',
    primary key (dept_id)
) engine = innodb
  auto_increment = 1 comment = '部门表';

-- 初始化部门表
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`,
                        `email`, `status`, `create_by`, `create_time`, `update_by`, `update_time`)
VALUES (1, 0, '0', '厦门慧成科技有限公司', 0, '高建平', '15888888888', 'hc@qq.com', '0', 'admin', sysdate(), '',
        NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`,
                        `email`, `status`, `create_by`, `create_time`, `update_by`, `update_time`)
VALUES (2, 0, '0', '厦门莲花灯科技管理有限公司', 0, '兮竹', '15888888888', 'xyt@qq.com', '0', 'xyt', sysdate(), '',
        NULL);

-- 用户信息表
drop table if exists sys_user;
create table sys_user
(
    user_id     bigint(20)  not null auto_increment comment '用户ID',
    dept_id     bigint(20)   default null comment '部门ID',
    user_name   varchar(30) not null comment '用户账号',
    nick_name   varchar(30) not null comment '用户昵称',
    email       varchar(50)  default '' comment '用户邮箱',
    phonenumber varchar(11)  default '' comment '手机号码',
    sex         char(1)      default '0' comment '用户性别（0男 1女 2未知）',
    avatar      varchar(100) default '' comment '头像地址',
    password    varchar(100) default '' comment '密码',
    status      char(1)      default '0' comment '帐号状态（0正常 1停用）',
    login_ip    varchar(128) default '' comment '最后登录IP',
    login_date  datetime comment '最后登录时间',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (user_id)
) engine = innodb
  auto_increment = 1 comment = '用户信息表';

-- 初始化用户表
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `email`, `phonenumber`, `sex`,
                        `avatar`, `password`, `status`, `login_ip`, `login_date`, `create_by`, `create_time`,
                        `update_by`, `update_time`, `remark`)
VALUES (1, 1, 'admin', '辣条', 'pig@163.com', '15888888888', '1', '/assets/img/avatar.jpg',
        '14e1b600b1fd579f47433b88e8d85291', '0', '192.155.0.1', '2023-03-09 17:52:20', 'admin', '2023-02-25 14:22:34',
        NULL, '2023-03-09 17:52:20', null);
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `email`, `phonenumber`, `sex`,
                        `avatar`, `password`, `status`, `login_ip`, `login_date`, `create_by`, `create_time`,
                        `update_by`, `update_time`, `remark`)
VALUES (2, 2, 'xyt', '辣条', 'pig@163.com', '15888888888', '1', '/assets/img/avatar.jpg',
        '14e1b600b1fd579f47433b88e8d85291', '0', '192.155.0.1', '2023-03-09 17:52:20', 'admin', '2023-02-25 14:22:34',
        NULL, '2023-03-09 17:52:20', null);

-- 岗位信息表
drop table if exists sys_post;
create table sys_post
(
    post_id     bigint(20)  not null auto_increment comment '岗位ID',
    dept_id     bigint(20)   default null comment '部门ID',
    post_code   varchar(64) not null comment '岗位编码',
    post_name   varchar(50) not null comment '岗位名称',
    post_sort   int(4)      not null comment '显示顺序',
    status      char(1)     not null comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (post_id)
) engine = innodb
  auto_increment = 1 comment = '岗位信息表';

-- 初始化岗位表
INSERT INTO `sys_post` (`post_id`, `dept_id`, `post_code`, `post_name`, `post_sort`, `status`, `create_by`,
                        `create_time`, `update_by`, `update_time`, `remark`)
VALUES (1, 1, 'ceo', '董事长', 99, '0', 'admin', '2023-02-24 11:57:04', '', NULL, '');

-- 角色信息表
drop table if exists sys_role;
create table sys_role
(
    role_id             bigint(20)   not null auto_increment comment '角色ID',
    role_name           varchar(30)  not null comment '角色名称',
    role_key            varchar(100) not null comment '角色权限字符串',
    role_sort           int(4)       not null comment '显示顺序',
    data_scope          char(1)      default '1' comment '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限 5仅本人数据权限）',
    menu_check_strictly tinyint(1)   default 1 comment '菜单树选择项是否关联显示',
    dept_check_strictly tinyint(1)   default 1 comment '部门树选择项是否关联显示',
    status              char(1)      not null comment '角色状态（0正常 1停用）',
    create_by           varchar(64)  default '' comment '创建者',
    create_time         datetime comment '创建时间',
    update_by           varchar(64)  default '' comment '更新者',
    update_time         datetime comment '更新时间',
    remark              varchar(500) default null comment '备注',
    primary key (role_id)
) engine = innodb
  auto_increment = 1 comment = '角色信息表';

-- 初始化角色表
INSERT INTO `sys_role` (`role_id`, `role_name`, `role_key`, `role_sort`, `data_scope`,
                        `menu_check_strictly`, `dept_check_strictly`, `status`, `create_by`, `create_time`, `update_by`,
                        `update_time`, `remark`)
VALUES (1, '超级管理员', 'admin', 99, '1', 1, 1, '0', 'admin', sysdate(), '', NULL, null);
INSERT INTO `sys_role` (`role_id`, `role_name`, `role_key`, `role_sort`, `data_scope`,
                        `menu_check_strictly`, `dept_check_strictly`, `status`, `create_by`, `create_time`, `update_by`,
                        `update_time`, `remark`)
VALUES (2, '管理员', 'manage', 99, '4', 1, 1, '0', 'xyt', sysdate(), '', null, NULL);

-- 系统应用表
drop table if exists sys_app;
create table sys_app
(
    app_id      bigint(20)   not null auto_increment comment '应用ID',
    app_key     varchar(30)  not null comment '应用标识',
    app_name    varchar(100) not null comment '应用名称',
    create_by   varchar(64) default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64) default '' comment '更新者',
    update_time datetime comment '更新时间',
    primary key (app_id)
) engine = innodb
  auto_increment = 1 comment = '系统应用表';

-- 初始化系统应用表
insert into sys_app (`app_id`, `app_key`, `app_name`, `create_by`, `create_time`)
values (1, 'ADMIN', '后台', 'admin', sysdate());
insert into sys_app (`app_id`, `app_key`, `app_name`, `create_by`, `create_time`)
values (2, 'APP', 'APP', 'admin', sysdate());

-- 菜单表
drop table if exists sys_menu;
create table sys_menu
(
    menu_id     bigint(20)  not null auto_increment comment '菜单ID',
    parent_id   bigint(20)   default 0 comment '父菜单ID',
    app_key     varchar(30) not null comment '应用标识',
    menu_name   varchar(50) not null comment '菜单名称',
    locale      varchar(50) not null comment '多语言标识',
    route_name  varchar(30) not null comment '路由名称',
    order_num   int(4)       default 99 comment '显示顺序',
    path        varchar(200) default null comment '路由地址',
    layout      varchar(30)  default null comment '页面布局（none DefaultLayout PageLayout）',
    component   varchar(255) default null comment '组件路径',
    query       varchar(255) default null comment '路由参数',
    is_frame    int(1)       default 1 comment '是否为外链（0是 1否）',
    is_cache    int(1)       default 0 comment '是否缓存（0缓存 1不缓存）',
    menu_type   int(1)       default 1 comment '菜单类型（0目录 1菜单）',
    visible     char(1)      default 0 comment '菜单状态（0显示 1隐藏）',
    status      char(1)      default 0 comment '菜单状态（0正常 1停用）',
    perms       varchar(100) default null comment '权限标识',
    icon        varchar(100) default null comment '菜单图标',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default '' comment '备注',
    primary key (menu_id)
) engine = innodb
  auto_increment = 1 comment = '菜单表';

-- 初始化菜单表 todo
INSERT INTO `sys_menu` VALUES (1, 0, 'ADMIN', '系统管理', '系统管理', 'LgunTP', 99, '', 'DefaultLayout', '', '', 1, 0, 0, '0', '0', '', 'icon-home', 'admin', '2023-03-10 18:30:52', '', '2023-03-10 18:30:52', NULL);
INSERT INTO `sys_menu` VALUES (3, 1, 'ADMIN', '产品管理', '产品管理', 'qRVDsI', 99, '/system/product', 'DefaultLayout', 'system/product/index', '', 1, 0, 1, '0', '0', 'systetm:product:list', 'icon-home', 'admin', '2023-03-10 18:58:50', 'admin', '2023-03-11 09:41:39', NULL);
INSERT INTO `sys_menu` VALUES (4, 1, 'ADMIN', '功能管理', '功能管理', 'qfJjsI', 99, '/system/feature', 'DefaultLayout', 'system/feature/index', '', 1, 0, 1, '0', '0', 'systetm:feature:list', 'icon-home', 'admin', '2023-03-10 18:58:50', 'admin', '2023-03-11 09:41:39', NULL);
INSERT INTO `sys_menu` VALUES (5, 1, 'ADMIN', '应用管理', '应用管理', 'CgWfJj', 99, 'system/app', 'DefaultLayout', 'system/app/index', '', 1, 0, 1, '0', '0', 'system:app:list', 'icon-home', 'admin', '2023-03-10 19:00:11', 'admin', '2023-03-10 19:01:03', NULL);
INSERT INTO `sys_menu` VALUES (6, 0, 'ADMIN', '组织架构', '组织架构', 'CuDcsY', 99, '', 'DefaultLayout', '', '', 1, 0, 0, '0', '0', '', 'icon-home', 'admin', '2023-03-10 19:49:25', '', '2023-03-10 19:49:25', NULL);
INSERT INTO `sys_menu` VALUES (7, 6, 'ADMIN', '用户管理', '用户管理', 'DCjYoV', 99, '/org/user', 'DefaultLayout', 'org/user/index', '', 1, 0, 1, '0', '0', 'org:user:list', 'icon-home', 'admin', '2023-03-10 19:53:02', '', '2023-03-10 19:53:02', NULL);
INSERT INTO `sys_menu` VALUES (8, 6, 'ADMIN', '角色管理', '角色管理', 'xvgOeA', 99, '/org/role', 'DefaultLayout', 'org/role/index', '', 1, 0, 1, '0', '0', 'org:role:list', 'icon-home', 'admin', '2023-03-10 20:07:04', '', '2023-03-10 20:07:04', NULL);
INSERT INTO `sys_menu` VALUES (9, 6, 'ADMIN', '部门管理', '部门管理', 'xecLYl', 99, '/org/dept', 'DefaultLayout', 'org/dept/index', '', 1, 0, 1, '0', '0', 'org:dept:list', 'icon-home', 'admin', '2023-03-10 20:09:31', '', '2023-03-10 20:09:31', NULL);
INSERT INTO `sys_menu` VALUES (10, 6, 'ADMIN', '岗位管理', '岗位管理', 'HpRzNy', 99, '/org/post', 'DefaultLayout', 'org/post/index', '', 1, 0, 1, '0', '0', 'org:post:list', 'icon-home', 'admin', '2023-03-10 20:11:23', '', '2023-03-10 20:11:23', NULL);
INSERT INTO `sys_menu` VALUES (11, 0, 'ADMIN', '客户管理', '客户管理', 'ldrbYT', 99, '', 'DefaultLayout', '', '', 1, 0, 0, '0', '0', '', 'icon-home', 'admin', '2023-03-10 20:14:20', 'admin', '2023-03-10 20:47:43', NULL);
INSERT INTO `sys_menu` VALUES (12, 11, 'ADMIN', '计划拜访', '计划拜访', 'TZslAB', 99, '/customer/plant', 'DefaultLayout', 'customer/Plannedvisit', '', 1, 0, 1, '0', '0', 'customer:plannedVisit', 'icon-home', 'admin', '2023-03-10 20:36:02', 'admin', '2023-03-10 20:38:28', NULL);
INSERT INTO `sys_menu` VALUES (13, 11, 'ADMIN', '我的客户', '我的客户', 'Xivnsw', 99, '/customer/manage', 'DefaultLayout', 'customer/Customermanagement', '', 1, 0, 1, '0', '0', 'customer:customerManagement', 'icon-home', 'admin', '2023-03-10 20:37:40', '', '2023-03-10 20:37:40', NULL);
INSERT INTO `sys_menu` VALUES (14, 11, 'ADMIN', '字段配置', '字段配置', 'VAjgMN', 99, '/customer/field', 'DefaultLayout', 'customer/field/index', '', 1, 0, 1, '0', '0', 'customer:field', 'icon-home', 'admin', '2023-03-10 20:40:45', '', '2023-03-10 20:40:45', NULL);
INSERT INTO `sys_menu` VALUES (15, 1, 'ADMIN', '访问日志', '访问日志', 'sycVKG', 99, '/system/login-log/index', 'DefaultLayout', 'system/login-log/index', '', 1, 0, 1, '0', '0', 'system:login-log:list', 'icon-home', 'admin', '2023-03-11 11:56:12', '', '2023-03-11 11:56:12', NULL);
INSERT INTO `sys_menu` VALUES (16, 1, 'ADMIN', '操作日志', '操作日志', 'tzEHKh', 99, '/system/operate-log/index', 'DefaultLayout', 'system/operate-log/index', '', 1, 0, 1, '0', '0', 'system:operate-log:list', 'icon-home', 'admin', '2023-03-11 12:00:36', '', '2023-03-11 12:00:36', NULL);
INSERT INTO `sys_menu` VALUES (17, 1, 'ADMIN', '公告管理', '公告管理', 'yrQXqO', 99, '/system/announcement/index', 'DefaultLayout', 'system/announcement/index', '', 1, 0, 1, '0', '0', 'system:announcement:index', 'icon-home', 'admin', '2023-03-11 13:47:49', '', '2023-03-11 13:47:49', NULL);
INSERT INTO `sys_menu` VALUES (18, 1, 'ADMIN', '附件管理', '附件管理', 'cPZoUq', 99, '/system/appendix/index', 'DefaultLayout', 'system/appendix/index', '', 1, 0, 1, '0', '0', 'system:appendix:index', 'icon-home', 'admin', '2023-03-11 13:55:48', '', '2023-03-11 13:55:48', NULL);

-- 权限表
drop table if exists sys_permission;
create table sys_permission
(
    permission_id bigint(20)  not null auto_increment comment '权限ID',
    parent_id     bigint(20)   default 0 comment '父菜单ID',
    app_key       varchar(30) not null comment '应用标识',
    name          varchar(50) not null comment '名称',
    order_num     int(4)       default 99 comment '显示顺序',
    status        char(1)      default 0 comment '菜单状态（0正常 1停用）',
    perms         varchar(100) default null comment '权限标识',
    create_by     varchar(64)  default '' comment '创建者',
    create_time   datetime comment '创建时间',
    update_by     varchar(64)  default '' comment '更新者',
    update_time   datetime comment '更新时间',
    remark        varchar(500) default '' comment '备注',
    primary key (`permission_id`)
) engine = innodb
  auto_increment = 1 comment ='权限表';

-- 初始化权限表 todo
INSERT INTO `sys_permission` VALUES (1, 0, 'ADMIN', '系统管理', 99, '0', '', '123123', '2023-03-10 23:13:50', 'admin', '2023-03-10 23:25:16', NULL);
INSERT INTO `sys_permission` VALUES (2, 1, 'ADMIN', '应用管理', 99, '0', '', '123123', '2023-03-10 23:14:25', '123123', '2023-03-10 23:19:40', NULL);
INSERT INTO `sys_permission` VALUES (3, 2, 'ADMIN', '查询', 99, '0', 'admin:app:list', '123123', '2023-03-10 23:15:43', 'admin', '2023-03-11 00:29:42', NULL);
INSERT INTO `sys_permission` VALUES (4, 2, 'ADMIN', '删除', 99, '0', 'admin:app:del', 'admin', '2023-03-11 00:29:38', '', '2023-03-11 00:29:38', NULL);
INSERT INTO `sys_permission` VALUES (5, 2, 'ADMIN', '编辑', 99, '0', 'admin:app:update', 'admin', '2023-03-11 00:29:57', 'admin', '2023-03-11 00:30:18', NULL);
INSERT INTO `sys_permission` VALUES (6, 2, 'ADMIN', '创建', 99, '0', 'admin:app:create', 'admin', '2023-03-11 00:30:12', '', '2023-03-11 00:30:12', NULL);
INSERT INTO `sys_permission` VALUES (7, 2, 'ADMIN', '菜单管理', 99, '0', 'admin:app:menu', 'admin', '2023-03-11 00:30:54', '', '2023-03-11 00:30:54', NULL);
INSERT INTO `sys_permission` VALUES (8, 2, 'ADMIN', '权限管理', 99, '0', 'admin:app:perm', 'admin', '2023-03-11 00:31:12', '', '2023-03-11 00:31:12', NULL);

-- 用户和角色关联表  用户N-N角色
drop table if exists sys_user_role;
create table sys_user_role
(
    user_id bigint(20) not null comment '用户ID',
    role_id bigint(20) not null comment '角色ID',
    primary key (user_id, role_id)
) engine = innodb comment = '用户和角色关联表';

-- 初始化用户和角色关联表
INSERT INTO `sys_user_role` (`user_id`, `role_id`)
VALUES (1, 1);
INSERT INTO `sys_user_role` (`user_id`, `role_id`)
VALUES (2, 2);

-- 角色和菜单关联表  角色1-N菜单
drop table if exists sys_role_menu;
create table sys_role_menu
(
    role_id bigint(20) not null comment '角色ID',
    menu_id bigint(20) not null comment '菜单ID',
    primary key (role_id, menu_id)
) engine = innodb comment = '角色和菜单关联表';

-- 初始化角色和菜单关联表 todo

-- 角色和权限关联表  角色1-N菜单
drop table if exists sys_role_permission;
create table sys_role_permission
(
    role_id       bigint(20) not null comment '角色ID',
    permission_id bigint(20) not null comment '权限ID',
    primary key (role_id, permission_id)
) engine = innodb comment = '角色和权限关联表';

-- 初始化角色和权限关联表 todo

-- 角色和部门关联表  角色1-N部门
drop table if exists sys_role_dept;
create table sys_role_dept
(
    role_id bigint(20) not null comment '角色ID',
    dept_id bigint(20) not null comment '部门ID',
    primary key (role_id, dept_id)
) engine = innodb comment = '角色和部门关联表';

-- 初始化角色和部门关联表 todo

-- 用户与岗位关联表  用户1-N岗位
drop table if exists sys_user_post;
create table sys_user_post
(
    user_id bigint(20) not null comment '用户ID',
    post_id bigint(20) not null comment '岗位ID',
    primary key (user_id, post_id)
) engine = innodb comment = '用户与岗位关联表';

-- 初始化角色和部门关联表 todo
