-- 直通车账号绑定表
drop table if exists ztc_user;
create table ztc_user
(
    ztc_user_id    bigint(20)   not null auto_increment comment 'ID',
    user_id        bigint(20)   not null comment '关联用户ID',
    username       varchar(100) not null comment '账号',
    password       text         not null comment '密码',
    expire         tinyint(1)   not null default 0 comment '登录失效 0未失效 1已失效',
    last_sync_time datetime              default null comment '最后同步数据时间',
    primary key (ztc_user_id)
) engine = innodb
  auto_increment = 1 comment = '直通车账号绑定表';