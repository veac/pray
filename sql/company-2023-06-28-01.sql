-- 企业表
drop table if exists company;
create table company
(
    company_id   bigint(20)   not null comment 'ID',
    user_id      bigint(20)   not null comment '关联用户',
    leader       varchar(50)  not null comment '挂钩领导',
    company_name varchar(100) not null comment '企业名称',
    company_code varchar(100) not null comment '统一社会信用代码',
    mobiles      varchar(100) default '' comment '联系电话 ,分割',
    area         varchar(50)  not null comment '所属区域',
    visit_num    int(10)      default 0 comment '走访次数',
    create_by    varchar(64)  default '' comment '创建者',
    create_time  datetime comment '创建时间',
    update_by    varchar(64)  default '' comment '更新者',
    update_time  datetime comment '更新时间',
    primary key (company_id)
) engine = innodb comment = '企业表';