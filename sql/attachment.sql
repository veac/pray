-- 附件表
drop table if exists attachment;
create table attachment
(
    attachment_id   bigint(20) not null auto_increment comment 'ID',
    parent_id       bigint(20) default 0 comment '父ID',
    attachment_type int(1) default 2 comment '类型(1目录 2文件)',
    path             varchar(255) not null comment '物理路径',
    file_type       varchar(30)  default 'file' comment '文件类型',
    file_name       varchar(100) default null comment '文件名称',
    file_size       varchar(50)  default '0' comment '文件大小',
    mimetype        varchar(100) default null comment 'mime类型',
    disk         varchar(100) default 'public' comment '储存位置(public, oss)',
    sha1            varchar(50)  not null unique comment 'SHA1值',
    extra           varchar(255) default null comment '参数信息',
    create_by       varchar(64)  default '' comment '创建者',
    create_time     datetime comment '创建时间',
    update_by       varchar(64)  default '' comment '更新者',
    update_time     datetime comment '更新时间',
    primary key (attachment_id)
) engine = innodb
  auto_increment = 1 comment = '附件表';
